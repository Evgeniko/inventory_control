<?
header('Access-Control-Allow-Origin: *');
$config = require_once '../../config/amo.php';
$configJSON = json_encode($config);
$URI = $config['siteURI'];
?>


<div id="inventory-control">
    <link rel="stylesheet" href="<?php echo $URI?>resources/css/sweetalert.css">
    <link rel="stylesheet" href="<?php echo $URI?>resources/css/easy-autocomplete.min.css">
    <script>
        function inventory_control() {
            $(document).ready(function () {
                document.addEventListener('render', function () {
                    widget();
                });
                document.addEventListener('nova_ready', function () {
                    widget();
                });
            });

            function widget() {
                <?
                header('Access-Control-Allow-Origin: *');
                $config = require_once '../../config/amo.php';
                $configJSON = json_encode($config);
                $URI = $config['siteURI'];
                ?>
                var UrlHelper = {
                    // ПОЛУЧЕНИЕ GET ПАРАМЕТРА ИЗ url
                    // вернет false если нет такого параметра
                    GET: function (key) {
                        var s = window.location.search;
                        s = s.match(new RegExp(key + '=([^&=]+)'));
                        return s ? s[1] : false;
                    }
                };

                var configJSON = '<?php echo $configJSON?>';
                var WIDGETCONFIG = JSON.parse(configJSON);
                console.log('Конфигурация');
                console.log(WIDGETCONFIG);
                var URI = WIDGETCONFIG.siteURI;
                var user_id = AMOCRM.constant('user').id;
                var current_entity = AMOCRM.data.current_entity;
                var is_card = AMOCRM.data.is_card;
                var subdomain = AMOCRM.constant('account').subdomain;
                var goodsTabId = WIDGETCONFIG.goodsTabId;
                var servicesTabId = WIDGETCONFIG.servicesTabId;



                if ((current_entity == 'leads') && (is_card)){
                    var lead_id = AMOCRM.data.current_card.id;
                    //Отрисовываем таблицу редактирования товаров
                    $('div[data-id="'+goodsTabId+'"]').click(function () {
                        console.log('click');
                        renderTable();
                    });
                    $('div[data-id="'+servicesTabId+'"]').click(function () {
                        console.log('click');
                        renderTable();
                    });
                    //Если после перезагрузки страницы открыта эта вкладка
                    var tab_id = UrlHelper.GET('tab_id');
                    if (tab_id == goodsTabId){
                        renderTable();
                    }
                    if (tab_id == servicesTabId){
                        renderTable();
                    }

                    //Удаляем добавленное при клике на другие табы
                    $('#card_tabs').find('[data-id]').not('[data-id="' + goodsTabId + '"]').click(function () {
                        $('#goods_table_tab').remove();
                    });

                }

                //Отрисовка таблицы
                function renderTable() {
                    console.log('renderTable');
                    if (!$('#goods_table_tab').length) {
                        $('[data-id="' + goodsTabId + '"]').not('.js-card-tab').empty();
                        $('.linked-forms__group-wrapper[data-id="'+goodsTabId+'"]').append(
                            '<div id="goods_table_tab">'+
                            '<table id="goods-table" border="1" align="center">' +
                            '<caption>Товары</caption>' +
                            '<thead>'+
                            '<tr>' +
                            '<th>Серия</th>' +
                            '<th>Номер</th>' +
                            '<th>Реестр</th>' +
                            '<th>Агент</th>' +
                            '<th></th>'+
                            '</tr>' +
                            '</thead>'+
                            '<tbody></tbody>'+
                            //'<tfoot>' +
                            //'<tr><td colspan="3">Итого</td><td colspan="2" id="table-sum-total">0</td></tr>'+
                            //'</tfoot>'+
                            '</table>'+
                            '<div style="padding-top: 10px">' +
                            '<input id="goods-autocomplete" class="add-input">'+
                            '<input type="hidden" id="selected-hidden-values" attr-id="" attr-series="" attr-number="" attr-reestr="" attr-agent="">'+
                            '<div class="add-btn" id="add-element-in-table">Добавить</div>'+
                            '</div>' +
                            '<div></div>' +
                            '<div id="save-table" class="btn js-save">Сохранить</div>'+
                            '</div>'
                        );
                        var elem = $('#goods-table');
                        elem[0].style.setProperty('text-align', 'center', 'important');
                        addValuesInTable();
                        initAutocomplete();
                    }
                }
            }
        }

        //Функция удаления значения из таблицы(строки)
        function delRowTable(column) {
            $(column).closest('tr').remove();
            calculateAllSum();
        }

        inventory_control();
        $(function(){

            render();

            function render() {
                if ($('#stock-widget-nova-btn').length == 0){
                    $('.nav__menu-wrapper [data-entity="catalogs"]').after(
                        '<div id="stock-widget-nova-btn" class="nav__menu__item nav__menu__item-selected" data-entity="stock">' +
                            '<a class="nav__menu__item__link" href="/">' +
                                '<div class="nav__menu__item__icon icon-catalogs">' +
                                    '<span class="js-notifications_counter nav__notifications__counter" style="display: none"></span>' +
                                '</div>' +
                                '<div class="nav__menu__item__title">Склад</div>' +
                            '</a>' +
                        '</div>'
                    );
                    $('#stock-widget-nova-btn').click(function () {
                        if ($('#aside-hover-nova-pip').length){
                            $('#aside-hover-nova-pip').remove();
                        }else{
                            $.get(URI+'stock/all', {}, function(data){
                                console.log(data);
                                var stocksHTML = '<li class="aside__list-item"><a id="history-view" class="aside__list-item-link">История</a></li>';
                                _.each(data, function (elem, i) {
                                    stocksHTML+='<li class="aside__list-item"><a id="'+elem.id+'" class="aside__list-item-link pip-item-nova">'+elem.name+'</a><span class="del_stock" style="color: red;" attr-id="'+elem.id+'">X</span></li>';
                                });
                                stocksHTML += '<li class="aside__list-item"><a id="0" class="aside__list-item-link pip-item-nova">'+'Все склады'+'</a></li>';
                                var addStockHTML = '<div id="add-stock-input" class="aside__footer aside__footer-add"><div class="aside__footer-inner js-aside__footer-add"><span class="aside__list-item-text h-text-overflow">Добавить склад</span><span class="aside__button aside__button_action " title="Добавить склад"><svg class="svg-icon svg-controls--button-add-dims"><use xlink:href="#controls--button-add"></use></svg></span></div></div>';
                                $('.nav__notifications').after(
                                    '<div class="aside aside-toggleable aside-hover aside-hover-catalogs aside-visible aside-expanded" id="aside-hover-nova-pip">\n' +
                                    '    <div class="aside__top">\n' +
                                    '        <h2 class="aside__head">Склады</h2>\n' +
                                    '    </div>\n' +
                                    '    <div class="aside__inner aside__inner_has-footer">\n' +
                                    '        <div class="aside__common-settings custom-scroll">\n' +
                                    '            <div class="aside__common-settings__list_wrapper" id="aside__list-wrapper">\n' +
                                    '                <ul class="aside__list">\n' +
                                    stocksHTML +
                                    '                </ul>\n' +
                                    '            </div>\n' +
                                    '        </div>\n' +
                                    '    </div>\n' +
                                    addStockHTML+
                                    '</div>'
                                );


                                //Добавление склада
                                $('#add-stock-input').click(function () {
                                    swal({
                                            title: "Добавление склада",
                                            text: "Введите имя склада:",
                                            type: "input",
                                            showCancelButton: true,
                                            closeOnConfirm: false,
                                            animation: "slide-from-top",
                                            inputPlaceholder: "Введите имя"
                                        },
                                        function(inputValue){
                                            if (inputValue === false) return false;

                                            if (inputValue === "") {
                                                swal.showInputError("Имя не должно быть пустым");
                                                return false
                                            }
                                            $.post(URI+'stock/add', {name: inputValue}, function (res) {
                                                if (res == 1){
                                                    swal("Добавлено", "Склад:" + inputValue+" был успешно добавлен", "success");
                                                } else {
                                                    swal("Ошибка", "Произошла ошибка при добавлении", "error");
                                                }

                                            });
                                        });

                                });

                                //Удалениие склада
                                $('.del_stock').click(function () {
                                    swal({
                                            title: "Подтверждение",
                                            text: "Вы действительно хотите удалить выбранный склад и все его элементы?",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Да, удалить",
                                            cancelButtonText: "Нет, вернуться",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                $.post(URI+'stock/delete', {id: id}, function (res) {
                                                    if (res == 1){
                                                        swal("Удалено", "Выбранные вами склад и его элементы были успешно удалены", "success");
                                                    }else {
                                                        swal("Ошибка", "Произошла ошибка при удалении", "error");
                                                    }
                                                });
                                            } else {
                                                swal("Отмена", "Удаление было отмененно", "error");
                                            }
                                        });
                                    var id = $(this).attr('attr-id');
                                });
                                $('.pip-item-nova').click(function () {
                                    var stock = {};
                                    stock['id'] = $(this).attr('id');
                                    stock['name'] = $(this).html();
                                    $('#aside-hover-nova-pip').remove();
                                    var pageHolder = $('#page_holder');
                                    if($('#page_holder').css('display') == 'none')
                                    {
                                        pageHolder = $('#card_holder');
                                    }
                                    console.log(pageHolder);
                                    //Отрисовка frame таблицы товаров со склада
                                    $(pageHolder).empty();
                                    $(pageHolder).append(' <iframe id="admin-panel-frame" name="target" src="'+URI+'views/stock/stockTableFrame.php" width="100%" height="1000px" align="left">Ваш браузер не поддерживает плавающие фреймы!</iframe>');
                                    //Отправка данных после загрузки iframe
                                    $('#admin-panel-frame').load(function () {
                                        var win = window.frames.target;
                                        win.postMessage(JSON.stringify(stock), '*');
                                    });
                                });

                                $('#history-view').click(function () {
                                    $('#aside-hover-nova-pip').remove();
                                    var pageHolder = $('#page_holder');
                                    if($('#page_holder').css('display') == 'none')
                                    {
                                        pageHolder = $('#card_holder');
                                    }
                                    $(pageHolder).empty();
                                    $(pageHolder).append(' <iframe id="history-panel" name="target" src="'+URI+'views/stock/history.php" width="100%" height="1000px" align="left">Ваш браузер не поддерживает плавающие фреймы!</iframe>');

                                });
                            });
                        }
                    });
                }
            }






            //Получение и добавление данных с сервера в таблицу
            function addValuesInTable() {
                var lead_id = AMOCRM.data.current_card.id;
                $.get(URI+'items/getReserved/'+lead_id, {}, function (items) {
                    var html = '';
                    _.each(items, function (item, i) {
                        html += '<tr>' +
                                '<input class="item-id" type="hidden" value="'+item.id+'">'+
                                '<td class="series-item">'+item.series+'</td>' +
                                '<td class="number-item">'+item.number+'</td>' +
                                '<td class="reestr-item">'+item.reestr+'</td>' +
                                '<td class="agent-item">'+item.agent+'</td>' +
                                '<td class="del-row-table" onclick="delRowTable(this)">X</td>' +
                            '</tr>';

                    });
                    $('#goods-table tbody').append(html);
                });
            }

            function initAutocomplete() {
                $.get(URI+"stock/allWithItemsActive", {}, function (stocks) {
                    console.log(stocks);
                    var elements = [];
                    _.each(stocks, function (stock, i) {
                        var items = stock.items;
                        _.each(items, function (item, i) {
                            var series = item.series? item.series: '';
                            var number = item.number? item.number: '';
                            var reestr = item.reestr? item.reestr: '';
                            var agent = item.agent? item.agent: '';
                            elements.push({
                                id: item.id,
                                name: series + " Номер: " + number + " Агент: " + agent,
                                series: series,
                                number: number,
                                reestr: reestr,
                                agent: agent
                            });
                        });
                    });
                    //Задаём опции для плагина
                    var options = {
                        data: elements,
                        placeholder: "Введите серию",
                        theme: "blue-light",
                        getValue: "name",
                        list: {
                            maxNumberOfElements: 999999,
                            match: {
                                enabled: true,
                                method: function(element, phrase) {
                                    return (element.lastIndexOf(phrase, 0) === 0);
                                }
                            },
                            onSelectItemEvent: function() {
                                var id = $("#goods-autocomplete").getSelectedItemData().id;
                                var series = $("#goods-autocomplete").getSelectedItemData().series;
                                var number = $("#goods-autocomplete").getSelectedItemData().number;
                                var reestr = $("#goods-autocomplete").getSelectedItemData().reestr;
                                var agent = $("#goods-autocomplete").getSelectedItemData().agent;
                                $("#selected-hidden-values").attr('attr-id', id);
                                $("#selected-hidden-values").attr('attr-series', series);
                                $("#selected-hidden-values").attr('attr-number', number);
                                $("#selected-hidden-values").attr('attr-reestr', reestr);
                                $("#selected-hidden-values").attr('attr-agent', agent);
                            }
                        }
                    };
                    $("#goods-autocomplete").easyAutocomplete(options);
                }, 'json');


                //Добавление даных в таблицу после выбора элемента из списка
                $('#add-element-in-table').click(function () {
                    var inputValue = $('#goods-autocomplete').val();
                    if (inputValue){
                        var html = '';
                        var id = $("#selected-hidden-values").attr('attr-id');
                        var number = $("#selected-hidden-values").attr('attr-number');
                        var series = $("#selected-hidden-values").attr('attr-series');
                        var reestr = $("#selected-hidden-values").attr('attr-reestr');
                        var agent = $("#selected-hidden-values").attr('attr-agent');
                        var name = series + " Номер: " + number + " Агент: " + agent;
                        if (name != inputValue){
                            alert('Выбран несуществующий элемент');
                            return false;
                        }
                        html += '<tr>' +
                            '<input class="item-id" type="hidden" value="'+id+'">'+
                            '<td class="series-item">'+series+'</td>' +
                            '<td class="number-item">'+number+'</td>' +
                            '<td class="reestr-item">'+reestr+'</td>' +
                            '<td class="agent-item">'+agent+'</td>' +
                            '<td class="del-row-table" onclick="delRowTable(this)">X</td>' +
                            '</tr>';
                        $('#goods-table tbody').append(html);
                        clearAutocompleteValue();
                    }
                });
            }


            function clearAutocompleteValue() {
                $('#goods-autocomplete').val('');
                $("#selected-hidden-values").attr('attr-id', '');
                $("#selected-hidden-values").attr('attr-number', '');
                $("#selected-hidden-values").attr('attr-series', '');
                $("#selected-hidden-values").attr('attr-reestr', '');
                $("#selected-hidden-values").attr('attr-agent', '');
            }

            $('.linked-forms__group-wrapper[data-id="'+goodsTabId+'"]').delegate('#save-table', 'click', function () {
                saveTable();
            });
            //Сохранение таблицы на сервере
            function saveTable() {
                var lead_id = AMOCRM.data.current_card.id;
                console.log('Сохранение');
                var ids = [];
                $('#goods-table > tbody > tr').each(function (i, elem) {
                    var id = $(elem).find('.item-id').val();
                    ids.push(id);
                });
                console.log(ids);
                $.post(URI+'items/reserved', {ids: ids, lead_id: lead_id}, function (res) {
                    console.log(res);
                    $('#save-table').css('background-color', 'green');
                    $('#save-table').html('Сохранено');
                    /*var currentTimestamp = Math.floor(Date.now() / 1000);
                    var update= {
                        update: [
                            {
                                id: lead_id,
                                updated_at: currentTimestamp,
                                sale: allsum,
                            }
                        ]
                    };
                    $.post('/api/v2/leads', update, function () {
                        $('#save-table').css('background-color', 'green');
                        $('#save-table').html('Сохранено');
                        /!*$("#save-table").hide(200, function() {
                            $(this).html("Сохранить").show(1000);
                            $('#save-table').css('background-color', '#4c8bf7');
                        });*!/
                    });*/
                });
            }



            //SweetAlert
                !function(e,t,n){"use strict";!function o(e,t,n){function a(s,l){if(!t[s]){if(!e[s]){var i="function"==typeof require&&require;if(!l&&i)return i(s,!0);if(r)return r(s,!0);var u=new Error("Cannot find module '"+s+"'");throw u.code="MODULE_NOT_FOUND",u}var c=t[s]={exports:{}};e[s][0].call(c.exports,function(t){var n=e[s][1][t];return a(n?n:t)},c,c.exports,o,e,t,n)}return t[s].exports}for(var r="function"==typeof require&&require,s=0;s<n.length;s++)a(n[s]);return a}({1:[function(o){var a,r,s,l,i=function(e){return e&&e.__esModule?e:{"default":e}},u=o("./modules/handle-dom"),c=o("./modules/utils"),d=o("./modules/handle-swal-dom"),f=o("./modules/handle-click"),p=o("./modules/handle-key"),m=i(p),v=o("./modules/default-params"),y=i(v),h=o("./modules/set-params"),g=i(h);s=l=function(){function o(e){var t=s;return t[e]===n?y["default"][e]:t[e]}var s=arguments[0];if(u.addClass(t.body,"stop-scrolling"),d.resetInput(),s===n)return c.logStr("SweetAlert expects at least 1 attribute!"),!1;var l=c.extend({},y["default"]);switch(typeof s){case"string":l.title=s,l.text=arguments[1]||"",l.type=arguments[2]||"";break;case"object":if(s.title===n)return c.logStr('Missing "title" argument!'),!1;l.title=s.title;for(var i in y["default"])l[i]=o(i);l.confirmButtonText=l.showCancelButton?"Confirm":y["default"].confirmButtonText,l.confirmButtonText=o("confirmButtonText"),l.doneFunction=arguments[1]||null;break;default:return c.logStr('Unexpected type of argument! Expected "string" or "object", got '+typeof s),!1}g["default"](l),d.fixVerticalPosition(),d.openModal(arguments[1]);for(var p=d.getModal(),v=p.querySelectorAll("button"),h=["onclick","onmouseover","onmouseout","onmousedown","onmouseup","onfocus"],b=function(e){return f.handleButton(e,l,p)},w=0;w<v.length;w++)for(var C=0;C<h.length;C++){var S=h[C];v[w][S]=b}d.getOverlay().onclick=b,a=e.onkeydown;var x=function(e){return m["default"](e,l,p)};e.onkeydown=x,e.onfocus=function(){setTimeout(function(){r!==n&&(r.focus(),r=n)},0)}},s.setDefaults=l.setDefaults=function(e){if(!e)throw new Error("userParams is required");if("object"!=typeof e)throw new Error("userParams has to be a object");c.extend(y["default"],e)},s.close=l.close=function(){var o=d.getModal();u.fadeOut(d.getOverlay(),5),u.fadeOut(o,5),u.removeClass(o,"showSweetAlert"),u.addClass(o,"hideSweetAlert"),u.removeClass(o,"visible");var s=o.querySelector(".sa-icon.sa-success");u.removeClass(s,"animate"),u.removeClass(s.querySelector(".sa-tip"),"animateSuccessTip"),u.removeClass(s.querySelector(".sa-long"),"animateSuccessLong");var l=o.querySelector(".sa-icon.sa-error");u.removeClass(l,"animateErrorIcon"),u.removeClass(l.querySelector(".sa-x-mark"),"animateXMark");var i=o.querySelector(".sa-icon.sa-warning");return u.removeClass(i,"pulseWarning"),u.removeClass(i.querySelector(".sa-body"),"pulseWarningIns"),u.removeClass(i.querySelector(".sa-dot"),"pulseWarningIns"),setTimeout(function(){var e=o.getAttribute("data-custom-class");u.removeClass(o,e)},300),u.removeClass(t.body,"stop-scrolling"),e.onkeydown=a,e.previousActiveElement&&e.previousActiveElement.focus(),r=n,clearTimeout(o.timeout),!0},s.showInputError=l.showInputError=function(e){var t=d.getModal(),n=t.querySelector(".sa-input-error");u.addClass(n,"show");var o=t.querySelector(".sa-error-container");u.addClass(o,"show"),o.querySelector("p").innerHTML=e,t.querySelector("input").focus()},s.resetInputError=l.resetInputError=function(e){if(e&&13===e.keyCode)return!1;var t=d.getModal(),n=t.querySelector(".sa-input-error");u.removeClass(n,"show");var o=t.querySelector(".sa-error-container");u.removeClass(o,"show")},"undefined"!=typeof e?e.sweetAlert=e.swal=s:c.logStr("SweetAlert is a frontend module!")},{"./modules/default-params":2,"./modules/handle-click":3,"./modules/handle-dom":4,"./modules/handle-key":5,"./modules/handle-swal-dom":6,"./modules/set-params":8,"./modules/utils":9}],2:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o={title:"",text:"",type:null,allowOutsideClick:!1,showConfirmButton:!0,showCancelButton:!1,closeOnConfirm:!0,closeOnCancel:!0,confirmButtonText:"OK",confirmButtonColor:"#AEDEF4",cancelButtonText:"Cancel",imageUrl:null,imageSize:null,timer:null,customClass:"",html:!1,animation:!0,allowEscapeKey:!0,inputType:"text",inputPlaceholder:"",inputValue:""};n["default"]=o,t.exports=n["default"]},{}],3:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=t("./utils"),r=(t("./handle-swal-dom"),t("./handle-dom")),s=function(t,n,o){function s(e){m&&n.confirmButtonColor&&(p.style.backgroundColor=e)}var u,c,d,f=t||e.event,p=f.target||f.srcElement,m=-1!==p.className.indexOf("confirm"),v=-1!==p.className.indexOf("sweet-overlay"),y=r.hasClass(o,"visible"),h=n.doneFunction&&"true"===o.getAttribute("data-has-done-function");switch(m&&n.confirmButtonColor&&(u=n.confirmButtonColor,c=a.colorLuminance(u,-.04),d=a.colorLuminance(u,-.14)),f.type){case"mouseover":s(c);break;case"mouseout":s(u);break;case"mousedown":s(d);break;case"mouseup":s(c);break;case"focus":var g=o.querySelector("button.confirm"),b=o.querySelector("button.cancel");m?b.style.boxShadow="none":g.style.boxShadow="none";break;case"click":var w=o===p,C=r.isDescendant(o,p);if(!w&&!C&&y&&!n.allowOutsideClick)break;m&&h&&y?l(o,n):h&&y||v?i(o,n):r.isDescendant(o,p)&&"BUTTON"===p.tagName&&sweetAlert.close()}},l=function(e,t){var n=!0;r.hasClass(e,"show-input")&&(n=e.querySelector("input").value,n||(n="")),t.doneFunction(n),t.closeOnConfirm&&sweetAlert.close()},i=function(e,t){var n=String(t.doneFunction).replace(/\s/g,""),o="function("===n.substring(0,9)&&")"!==n.substring(9,10);o&&t.doneFunction(!1),t.closeOnCancel&&sweetAlert.close()};o["default"]={handleButton:s,handleConfirm:l,handleCancel:i},n.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],4:[function(n,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=function(e,t){return new RegExp(" "+t+" ").test(" "+e.className+" ")},s=function(e,t){r(e,t)||(e.className+=" "+t)},l=function(e,t){var n=" "+e.className.replace(/[\t\r\n]/g," ")+" ";if(r(e,t)){for(;n.indexOf(" "+t+" ")>=0;)n=n.replace(" "+t+" "," ");e.className=n.replace(/^\s+|\s+$/g,"")}},i=function(e){var n=t.createElement("div");return n.appendChild(t.createTextNode(e)),n.innerHTML},u=function(e){e.style.opacity="",e.style.display="block"},c=function(e){if(e&&!e.length)return u(e);for(var t=0;t<e.length;++t)u(e[t])},d=function(e){e.style.opacity="",e.style.display="none"},f=function(e){if(e&&!e.length)return d(e);for(var t=0;t<e.length;++t)d(e[t])},p=function(e,t){for(var n=t.parentNode;null!==n;){if(n===e)return!0;n=n.parentNode}return!1},m=function(e){e.style.left="-9999px",e.style.display="block";var t,n=e.clientHeight;return t="undefined"!=typeof getComputedStyle?parseInt(getComputedStyle(e).getPropertyValue("padding-top"),10):parseInt(e.currentStyle.padding),e.style.left="",e.style.display="none","-"+parseInt((n+t)/2)+"px"},v=function(e,t){if(+e.style.opacity<1){t=t||16,e.style.opacity=0,e.style.display="block";var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity+(new Date-n)/100,n=+new Date,+e.style.opacity<1&&setTimeout(o,t)});o()}e.style.display="block"},y=function(e,t){t=t||16,e.style.opacity=1;var n=+new Date,o=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){e.style.opacity=+e.style.opacity-(new Date-n)/100,n=+new Date,+e.style.opacity>0?setTimeout(o,t):e.style.display="none"});o()},h=function(n){if("function"==typeof MouseEvent){var o=new MouseEvent("click",{view:e,bubbles:!1,cancelable:!0});n.dispatchEvent(o)}else if(t.createEvent){var a=t.createEvent("MouseEvents");a.initEvent("click",!1,!1),n.dispatchEvent(a)}else t.createEventObject?n.fireEvent("onclick"):"function"==typeof n.onclick&&n.onclick()},g=function(t){"function"==typeof t.stopPropagation?(t.stopPropagation(),t.preventDefault()):e.event&&e.event.hasOwnProperty("cancelBubble")&&(e.event.cancelBubble=!0)};a.hasClass=r,a.addClass=s,a.removeClass=l,a.escapeHtml=i,a._show=u,a.show=c,a._hide=d,a.hide=f,a.isDescendant=p,a.getTopMargin=m,a.fadeIn=v,a.fadeOut=y,a.fireClick=h,a.stopEventPropagation=g},{}],5:[function(t,o,a){Object.defineProperty(a,"__esModule",{value:!0});var r=t("./handle-dom"),s=t("./handle-swal-dom"),l=function(t,o,a){var l=t||e.event,i=l.keyCode||l.which,u=a.querySelector("button.confirm"),c=a.querySelector("button.cancel"),d=a.querySelectorAll("button[tabindex]");if(-1!==[9,13,32,27].indexOf(i)){for(var f=l.target||l.srcElement,p=-1,m=0;m<d.length;m++)if(f===d[m]){p=m;break}9===i?(f=-1===p?u:p===d.length-1?d[0]:d[p+1],r.stopEventPropagation(l),f.focus(),o.confirmButtonColor&&s.setFocusStyle(f,o.confirmButtonColor)):13===i?("INPUT"===f.tagName&&(f=u,u.focus()),f=-1===p?u:n):27===i&&o.allowEscapeKey===!0?(f=c,r.fireClick(f,l)):f=n}};a["default"]=l,o.exports=a["default"]},{"./handle-dom":4,"./handle-swal-dom":6}],6:[function(n,o,a){var r=function(e){return e&&e.__esModule?e:{"default":e}};Object.defineProperty(a,"__esModule",{value:!0});var s=n("./utils"),l=n("./handle-dom"),i=n("./default-params"),u=r(i),c=n("./injected-html"),d=r(c),f=".sweet-alert",p=".sweet-overlay",m=function(){var e=t.createElement("div");for(e.innerHTML=d["default"];e.firstChild;)t.body.appendChild(e.firstChild)},v=function(e){function t(){return e.apply(this,arguments)}return t.toString=function(){return e.toString()},t}(function(){var e=t.querySelector(f);return e||(m(),e=v()),e}),y=function(){var e=v();return e?e.querySelector("input"):void 0},h=function(){return t.querySelector(p)},g=function(e,t){var n=s.hexToRgb(t);e.style.boxShadow="0 0 2px rgba("+n+", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"},b=function(n){var o=v();l.fadeIn(h(),10),l.show(o),l.addClass(o,"showSweetAlert"),l.removeClass(o,"hideSweetAlert"),e.previousActiveElement=t.activeElement;var a=o.querySelector("button.confirm");a.focus(),setTimeout(function(){l.addClass(o,"visible")},500);var r=o.getAttribute("data-timer");if("null"!==r&&""!==r){var s=n;o.timeout=setTimeout(function(){var e=(s||null)&&"true"===o.getAttribute("data-has-done-function");e?s(null):sweetAlert.close()},r)}},w=function(){var e=v(),t=y();l.removeClass(e,"show-input"),t.value=u["default"].inputValue,t.setAttribute("type",u["default"].inputType),t.setAttribute("placeholder",u["default"].inputPlaceholder),C()},C=function(e){if(e&&13===e.keyCode)return!1;var t=v(),n=t.querySelector(".sa-input-error");l.removeClass(n,"show");var o=t.querySelector(".sa-error-container");l.removeClass(o,"show")},S=function(){var e=v();e.style.marginTop=l.getTopMargin(v())};a.sweetAlertInitialize=m,a.getModal=v,a.getOverlay=h,a.getInput=y,a.setFocusStyle=g,a.openModal=b,a.resetInput=w,a.resetInputError=C,a.fixVerticalPosition=S},{"./default-params":2,"./handle-dom":4,"./injected-html":7,"./utils":9}],7:[function(e,t,n){Object.defineProperty(n,"__esModule",{value:!0});var o='<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <button class="confirm" tabIndex="1">OK</button>\n    </div></div>';n["default"]=o,t.exports=n["default"]},{}],8:[function(e,t,o){Object.defineProperty(o,"__esModule",{value:!0});var a=e("./utils"),r=e("./handle-swal-dom"),s=e("./handle-dom"),l=["error","warning","info","success","input","prompt"],i=function(e){var t=r.getModal(),o=t.querySelector("h2"),i=t.querySelector("p"),u=t.querySelector("button.cancel"),c=t.querySelector("button.confirm");if(o.innerHTML=e.html?e.title:s.escapeHtml(e.title).split("\n").join("<br>"),i.innerHTML=e.html?e.text:s.escapeHtml(e.text||"").split("\n").join("<br>"),e.text&&s.show(i),e.customClass)s.addClass(t,e.customClass),t.setAttribute("data-custom-class",e.customClass);else{var d=t.getAttribute("data-custom-class");s.removeClass(t,d),t.setAttribute("data-custom-class","")}if(s.hide(t.querySelectorAll(".sa-icon")),e.type&&!a.isIE8()){var f=function(){for(var o=!1,a=0;a<l.length;a++)if(e.type===l[a]){o=!0;break}if(!o)return logStr("Unknown alert type: "+e.type),{v:!1};var i=["success","error","warning","info"],u=n;-1!==i.indexOf(e.type)&&(u=t.querySelector(".sa-icon.sa-"+e.type),s.show(u));var c=r.getInput();switch(e.type){case"success":s.addClass(u,"animate"),s.addClass(u.querySelector(".sa-tip"),"animateSuccessTip"),s.addClass(u.querySelector(".sa-long"),"animateSuccessLong");break;case"error":s.addClass(u,"animateErrorIcon"),s.addClass(u.querySelector(".sa-x-mark"),"animateXMark");break;case"warning":s.addClass(u,"pulseWarning"),s.addClass(u.querySelector(".sa-body"),"pulseWarningIns"),s.addClass(u.querySelector(".sa-dot"),"pulseWarningIns");break;case"input":case"prompt":c.setAttribute("type",e.inputType),c.value=e.inputValue,c.setAttribute("placeholder",e.inputPlaceholder),s.addClass(t,"show-input"),setTimeout(function(){c.focus(),c.addEventListener("keyup",swal.resetInputError)},400)}}();if("object"==typeof f)return f.v}if(e.imageUrl){var p=t.querySelector(".sa-icon.sa-custom");p.style.backgroundImage="url("+e.imageUrl+")",s.show(p);var m=80,v=80;if(e.imageSize){var y=e.imageSize.toString().split("x"),h=y[0],g=y[1];h&&g?(m=h,v=g):logStr("Parameter imageSize expects value with format WIDTHxHEIGHT, got "+e.imageSize)}p.setAttribute("style",p.getAttribute("style")+"width:"+m+"px; height:"+v+"px")}t.setAttribute("data-has-cancel-button",e.showCancelButton),e.showCancelButton?u.style.display="inline-block":s.hide(u),t.setAttribute("data-has-confirm-button",e.showConfirmButton),e.showConfirmButton?c.style.display="inline-block":s.hide(c),e.cancelButtonText&&(u.innerHTML=s.escapeHtml(e.cancelButtonText)),e.confirmButtonText&&(c.innerHTML=s.escapeHtml(e.confirmButtonText)),e.confirmButtonColor&&(c.style.backgroundColor=e.confirmButtonColor,r.setFocusStyle(c,e.confirmButtonColor)),t.setAttribute("data-allow-outside-click",e.allowOutsideClick);var b=e.doneFunction?!0:!1;t.setAttribute("data-has-done-function",b),e.animation?"string"==typeof e.animation?t.setAttribute("data-animation",e.animation):t.setAttribute("data-animation","pop"):t.setAttribute("data-animation","none"),t.setAttribute("data-timer",e.timer)};o["default"]=i,t.exports=o["default"]},{"./handle-dom":4,"./handle-swal-dom":6,"./utils":9}],9:[function(t,n,o){Object.defineProperty(o,"__esModule",{value:!0});var a=function(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e},r=function(e){var t=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);return t?parseInt(t[1],16)+", "+parseInt(t[2],16)+", "+parseInt(t[3],16):null},s=function(){return e.attachEvent&&!e.addEventListener},l=function(t){e.console&&e.console.log("SweetAlert: "+t)},i=function(e,t){e=String(e).replace(/[^0-9a-f]/gi,""),e.length<6&&(e=e[0]+e[0]+e[1]+e[1]+e[2]+e[2]),t=t||0;var n,o,a="#";for(o=0;3>o;o++)n=parseInt(e.substr(2*o,2),16),n=Math.round(Math.min(Math.max(0,n+n*t),255)).toString(16),a+=("00"+n).substr(n.length);return a};o.extend=a,o.hexToRgb=r,o.isIE8=s,o.logStr=l,o.colorLuminance=i},{}]},{},[1]),"function"==typeof define&&define.amd?define(function(){return sweetAlert}):"undefined"!=typeof module&&module.exports&&(module.exports=sweetAlert)}(window,document);




            /*
 * easy-autocomplete
 * jQuery plugin for autocompletion
 *
 * @author Łukasz Pawełczak (http://github.com/pawelczak)
 * @version 1.3.5
 * Copyright  License:
 */

            var EasyAutocomplete=function(a){return a.Configuration=function(a){function b(){if("xml"===a.dataType&&(a.getValue||(a.getValue=function(a){return $(a).text()}),a.list||(a.list={}),a.list.sort||(a.list.sort={}),a.list.sort.method=function(b,c){return b=a.getValue(b),c=a.getValue(c),c>b?-1:b>c?1:0},a.list.match||(a.list.match={}),a.list.match.method=function(a,b){return a.search(b)>-1}),void 0!==a.categories&&a.categories instanceof Array){for(var b=[],c=0,d=a.categories.length;d>c;c+=1){var e=a.categories[c];for(var f in h.categories[0])void 0===e[f]&&(e[f]=h.categories[0][f]);b.push(e)}a.categories=b}}function c(){function b(a,c){var d=a||{};for(var e in a)void 0!==c[e]&&null!==c[e]&&("object"!=typeof c[e]||c[e]instanceof Array?d[e]=c[e]:b(a[e],c[e]));return void 0!==c.data&&null!==c.data&&"object"==typeof c.data&&(d.data=c.data),d}h=b(h,a)}function d(){if("list-required"!==h.url&&"function"!=typeof h.url){var b=h.url;h.url=function(){return b}}if(void 0!==h.ajaxSettings.url&&"function"!=typeof h.ajaxSettings.url){var b=h.ajaxSettings.url;h.ajaxSettings.url=function(){return b}}if("string"==typeof h.listLocation){var c=h.listLocation;"XML"===h.dataType.toUpperCase()?h.listLocation=function(a){return $(a).find(c)}:h.listLocation=function(a){return a[c]}}if("string"==typeof h.getValue){var d=h.getValue;h.getValue=function(a){return a[d]}}void 0!==a.categories&&(h.categoriesAssigned=!0)}function e(){void 0!==a.ajaxSettings&&"object"==typeof a.ajaxSettings?h.ajaxSettings=a.ajaxSettings:h.ajaxSettings={}}function f(a){return void 0!==h[a]&&null!==h[a]}function g(a,b){function c(b,d){for(var e in d)void 0===b[e]&&a.log("Property '"+e+"' does not exist in EasyAutocomplete options API."),"object"==typeof b[e]&&-1===$.inArray(e,i)&&c(b[e],d[e])}c(h,b)}var h={data:"list-required",url:"list-required",dataType:"json",listLocation:function(a){return a},xmlElementName:"",getValue:function(a){return a},autocompleteOff:!0,placeholder:!1,ajaxCallback:function(){},matchResponseProperty:!1,list:{sort:{enabled:!1,method:function(a,b){return a=h.getValue(a),b=h.getValue(b),b>a?-1:a>b?1:0}},maxNumberOfElements:6,hideOnEmptyPhrase:!0,match:{enabled:!1,caseSensitive:!1,method:function(a,b){return a.search(b)>-1}},showAnimation:{type:"normal",time:400,callback:function(){}},hideAnimation:{type:"normal",time:400,callback:function(){}},onClickEvent:function(){},onSelectItemEvent:function(){},onLoadEvent:function(){},onChooseEvent:function(){},onKeyEnterEvent:function(){},onMouseOverEvent:function(){},onMouseOutEvent:function(){},onShowListEvent:function(){},onHideListEvent:function(){}},highlightPhrase:!0,theme:"",cssClasses:"",minCharNumber:0,requestDelay:0,adjustWidth:!0,ajaxSettings:{},preparePostData:function(a,b){return a},loggerEnabled:!0,template:"",categoriesAssigned:!1,categories:[{maxNumberOfElements:4}]},i=["ajaxSettings","template"];this.get=function(a){return h[a]},this.equals=function(a,b){return!(!f(a)||h[a]!==b)},this.checkDataUrlProperties=function(){return"list-required"!==h.url||"list-required"!==h.data},this.checkRequiredProperties=function(){for(var a in h)if("required"===h[a])return logger.error("Option "+a+" must be defined"),!1;return!0},this.printPropertiesThatDoesntExist=function(a,b){g(a,b)},b(),c(),h.loggerEnabled===!0&&g(console,a),e(),d()},a}(EasyAutocomplete||{}),EasyAutocomplete=function(a){return a.Logger=function(){this.error=function(a){console.log("ERROR: "+a)},this.warning=function(a){console.log("WARNING: "+a)}},a}(EasyAutocomplete||{}),EasyAutocomplete=function(a){return a.Constans=function(){var a={CONTAINER_CLASS:"easy-autocomplete-container",CONTAINER_ID:"eac-container-",WRAPPER_CSS_CLASS:"easy-autocomplete"};this.getValue=function(b){return a[b]}},a}(EasyAutocomplete||{}),EasyAutocomplete=function(a){return a.ListBuilderService=function(a,b){function c(b,c){function d(){var d,e={};return void 0!==b.xmlElementName&&(e.xmlElementName=b.xmlElementName),void 0!==b.listLocation?d=b.listLocation:void 0!==a.get("listLocation")&&(d=a.get("listLocation")),void 0!==d?"string"==typeof d?e.data=$(c).find(d):"function"==typeof d&&(e.data=d(c)):e.data=c,e}function e(){var a={};return void 0!==b.listLocation?"string"==typeof b.listLocation?a.data=c[b.listLocation]:"function"==typeof b.listLocation&&(a.data=b.listLocation(c)):a.data=c,a}var f={};if(f="XML"===a.get("dataType").toUpperCase()?d():e(),void 0!==b.header&&(f.header=b.header),void 0!==b.maxNumberOfElements&&(f.maxNumberOfElements=b.maxNumberOfElements),void 0!==a.get("list").maxNumberOfElements&&(f.maxListSize=a.get("list").maxNumberOfElements),void 0!==b.getValue)if("string"==typeof b.getValue){var g=b.getValue;f.getValue=function(a){return a[g]}}else"function"==typeof b.getValue&&(f.getValue=b.getValue);else f.getValue=a.get("getValue");return f}function d(b){var c=[];return void 0===b.xmlElementName&&(b.xmlElementName=a.get("xmlElementName")),$(b.data).find(b.xmlElementName).each(function(){c.push(this)}),c}this.init=function(b){var c=[],d={};return d.data=a.get("listLocation")(b),d.getValue=a.get("getValue"),d.maxListSize=a.get("list").maxNumberOfElements,c.push(d),c},this.updateCategories=function(b,d){if(a.get("categoriesAssigned")){b=[];for(var e=0;e<a.get("categories").length;e+=1){var f=c(a.get("categories")[e],d);b.push(f)}}return b},this.convertXml=function(b){if("XML"===a.get("dataType").toUpperCase())for(var c=0;c<b.length;c+=1)b[c].data=d(b[c]);return b},this.processData=function(c,d){for(var e=0,f=c.length;f>e;e+=1)c[e].data=b(a,c[e],d);return c},this.checkIfDataExists=function(a){for(var b=0,c=a.length;c>b;b+=1)if(void 0!==a[b].data&&a[b].data instanceof Array&&a[b].data.length>0)return!0;return!1}},a}(EasyAutocomplete||{}),EasyAutocomplete=function(a){return a.proccess=function(b,c,d){function e(a,c){var d=[],e="";if(b.get("list").match.enabled)for(var g=0,h=a.length;h>g;g+=1)e=b.get("getValue")(a[g]),f(e,c)&&d.push(a[g]);else d=a;return d}function f(a,c){return b.get("list").match.caseSensitive||("string"==typeof a&&(a=a.toLowerCase()),c=c.toLowerCase()),!!b.get("list").match.method(a,c)}function g(a){return void 0!==c.maxNumberOfElements&&a.length>c.maxNumberOfElements&&(a=a.slice(0,c.maxNumberOfElements)),a}function h(a){return b.get("list").sort.enabled&&a.sort(b.get("list").sort.method),a}a.proccess.match=f;var i=c.data,j=d;return i=e(i,j),i=g(i),i=h(i)},a}(EasyAutocomplete||{}),EasyAutocomplete=function(a){return a.Template=function(a){var b={basic:{type:"basic",method:function(a){return a},cssClass:""},description:{type:"description",fields:{description:"description"},method:function(a){return a+" - description"},cssClass:"eac-description"},iconLeft:{type:"iconLeft",fields:{icon:""},method:function(a){return a},cssClass:"eac-icon-left"},iconRight:{type:"iconRight",fields:{iconSrc:""},method:function(a){return a},cssClass:"eac-icon-right"},links:{type:"links",fields:{link:""},method:function(a){return a},cssClass:""},custom:{type:"custom",method:function(){},cssClass:""}},c=function(a){var c,d=a.fields;return"description"===a.type?(c=b.description.method,"string"==typeof d.description?c=function(a,b){return a+" - <span>"+b[d.description]+"</span>"}:"function"==typeof d.description&&(c=function(a,b){return a+" - <span>"+d.description(b)+"</span>"}),c):"iconRight"===a.type?("string"==typeof d.iconSrc?c=function(a,b){return a+"<img class='eac-icon' src='"+b[d.iconSrc]+"' />"}:"function"==typeof d.iconSrc&&(c=function(a,b){return a+"<img class='eac-icon' src='"+d.iconSrc(b)+"' />"}),c):"iconLeft"===a.type?("string"==typeof d.iconSrc?c=function(a,b){return"<img class='eac-icon' src='"+b[d.iconSrc]+"' />"+a}:"function"==typeof d.iconSrc&&(c=function(a,b){return"<img class='eac-icon' src='"+d.iconSrc(b)+"' />"+a}),c):"links"===a.type?("string"==typeof d.link?c=function(a,b){return"<a href='"+b[d.link]+"' >"+a+"</a>"}:"function"==typeof d.link&&(c=function(a,b){return"<a href='"+d.link(b)+"' >"+a+"</a>"}),c):"custom"===a.type?a.method:b.basic.method},d=function(a){return a&&a.type&&a.type&&b[a.type]?c(a):b.basic.method},e=function(a){var c=function(){return""};return a&&a.type&&a.type&&b[a.type]?function(){var c=b[a.type].cssClass;return function(){return c}}():c};this.getTemplateClass=e(a),this.build=d(a)},a}(EasyAutocomplete||{}),EasyAutocomplete=function(a){return a.main=function(b,c){function d(){return 0===t.length?void p.error("Input field doesn't exist."):o.checkDataUrlProperties()?o.checkRequiredProperties()?(e(),void g()):void p.error("Will not work without mentioned properties."):void p.error("One of options variables 'data' or 'url' must be defined.")}function e(){function a(){var a=$("<div>"),c=n.getValue("WRAPPER_CSS_CLASS");o.get("theme")&&""!==o.get("theme")&&(c+=" eac-"+o.get("theme")),o.get("cssClasses")&&""!==o.get("cssClasses")&&(c+=" "+o.get("cssClasses")),""!==q.getTemplateClass()&&(c+=" "+q.getTemplateClass()),a.addClass(c),t.wrap(a),o.get("adjustWidth")===!0&&b()}function b(){var a=t.outerWidth();t.parent().css("width",a)}function c(){t.unwrap()}function d(){var a=$("<div>").addClass(n.getValue("CONTAINER_CLASS"));a.attr("id",f()).prepend($("<ul>")),function(){a.on("show.eac",function(){switch(o.get("list").showAnimation.type){case"slide":var b=o.get("list").showAnimation.time,c=o.get("list").showAnimation.callback;a.find("ul").slideDown(b,c);break;case"fade":var b=o.get("list").showAnimation.time,c=o.get("list").showAnimation.callback;a.find("ul").fadeIn(b),c;break;default:a.find("ul").show()}o.get("list").onShowListEvent()}).on("hide.eac",function(){switch(o.get("list").hideAnimation.type){case"slide":var b=o.get("list").hideAnimation.time,c=o.get("list").hideAnimation.callback;a.find("ul").slideUp(b,c);break;case"fade":var b=o.get("list").hideAnimation.time,c=o.get("list").hideAnimation.callback;a.find("ul").fadeOut(b,c);break;default:a.find("ul").hide()}o.get("list").onHideListEvent()}).on("selectElement.eac",function(){a.find("ul li").removeClass("selected"),a.find("ul li").eq(w).addClass("selected"),o.get("list").onSelectItemEvent()}).on("loadElements.eac",function(b,c,d){var e="",f=a.find("ul");f.empty().detach(),v=[];for(var h=0,i=0,k=c.length;k>i;i+=1){var l=c[i].data;if(0!==l.length){void 0!==c[i].header&&c[i].header.length>0&&f.append("<div class='eac-category' >"+c[i].header+"</div>");for(var m=0,n=l.length;n>m&&h<c[i].maxListSize;m+=1)e=$("<li><div class='eac-item'></div></li>"),function(){var a=m,b=h,f=c[i].getValue(l[a]);e.find(" > div").on("click",function(){t.val(f).trigger("change"),w=b,j(b),o.get("list").onClickEvent(),o.get("list").onChooseEvent()}).mouseover(function(){w=b,j(b),o.get("list").onMouseOverEvent()}).mouseout(function(){o.get("list").onMouseOutEvent()}).html(q.build(g(f,d),l[a]))}(),f.append(e),v.push(l[m]),h+=1}}a.append(f),o.get("list").onLoadEvent()})}(),t.after(a)}function e(){t.next("."+n.getValue("CONTAINER_CLASS")).remove()}function g(a,b){return o.get("highlightPhrase")&&""!==b?i(a,b):a}function h(a){return a.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")}function i(a,b){var c=h(b);return(a+"").replace(new RegExp("("+c+")","gi"),"<b>$1</b>")}t.parent().hasClass(n.getValue("WRAPPER_CSS_CLASS"))&&(e(),c()),a(),d(),u=$("#"+f()),o.get("placeholder")&&t.attr("placeholder",o.get("placeholder"))}function f(){var a=t.attr("id");return a=n.getValue("CONTAINER_ID")+a}function g(){function a(){s("autocompleteOff",!0)&&n(),b(),c(),d(),e(),f(),g()}function b(){t.focusout(function(){var a,b=t.val();o.get("list").match.caseSensitive||(b=b.toLowerCase());for(var c=0,d=v.length;d>c;c+=1)if(a=o.get("getValue")(v[c]),o.get("list").match.caseSensitive||(a=a.toLowerCase()),a===b)return w=c,void j(w)})}function c(){t.off("keyup").keyup(function(a){function b(a){function b(){var a={},b=o.get("ajaxSettings")||{};for(var c in b)a[c]=b[c];return a}function c(a,b){return o.get("matchResponseProperty")!==!1?"string"==typeof o.get("matchResponseProperty")?b[o.get("matchResponseProperty")]===a:"function"==typeof o.get("matchResponseProperty")?o.get("matchResponseProperty")(b)===a:!0:!0}if(!(a.length<o.get("minCharNumber"))){if("list-required"!==o.get("data")){var d=o.get("data"),e=r.init(d);e=r.updateCategories(e,d),e=r.processData(e,a),k(e,a),t.parent().find("li").length>0?h():i()}var f=b();void 0!==f.url&&""!==f.url||(f.url=o.get("url")),void 0!==f.dataType&&""!==f.dataType||(f.dataType=o.get("dataType")),void 0!==f.url&&"list-required"!==f.url&&(f.url=f.url(a),f.data=o.get("preparePostData")(f.data,a),$.ajax(f).done(function(b){var d=r.init(b);d=r.updateCategories(d,b),d=r.convertXml(d),c(a,b)&&(d=r.processData(d,a),k(d,a)),r.checkIfDataExists(d)&&t.parent().find("li").length>0?h():i(),o.get("ajaxCallback")()}).fail(function(){p.warning("Fail to load response data")}).always(function(){}))}}switch(a.keyCode){case 27:i(),l();break;case 38:a.preventDefault(),v.length>0&&w>0&&(w-=1,t.val(o.get("getValue")(v[w])),j(w));break;case 40:a.preventDefault(),v.length>0&&w<v.length-1&&(w+=1,t.val(o.get("getValue")(v[w])),j(w));break;default:if(a.keyCode>40||8===a.keyCode){var c=t.val();o.get("list").hideOnEmptyPhrase!==!0||8!==a.keyCode||""!==c?o.get("requestDelay")>0?(void 0!==m&&clearTimeout(m),m=setTimeout(function(){b(c)},o.get("requestDelay"))):b(c):i()}}})}function d(){t.on("keydown",function(a){a=a||window.event;var b=a.keyCode;return 38===b?(suppressKeypress=!0,!1):void 0}).keydown(function(a){13===a.keyCode&&w>-1&&(t.val(o.get("getValue")(v[w])),o.get("list").onKeyEnterEvent(),o.get("list").onChooseEvent(),w=-1,i(),a.preventDefault())})}function e(){t.off("keypress")}function f(){t.focus(function(){""!==t.val()&&v.length>0&&(w=-1,h())})}function g(){t.blur(function(){setTimeout(function(){w=-1,i()},250)})}function n(){t.attr("autocomplete","off")}a()}function h(){u.trigger("show.eac")}function i(){u.trigger("hide.eac")}function j(a){u.trigger("selectElement.eac",a)}function k(a,b){u.trigger("loadElements.eac",[a,b])}function l(){t.trigger("blur")}var m,n=new a.Constans,o=new a.Configuration(c),p=new a.Logger,q=new a.Template(c.template),r=new a.ListBuilderService(o,a.proccess),s=o.equals,t=b,u="",v=[],w=-1;a.consts=n,this.getConstants=function(){return n},this.getConfiguration=function(){return o},this.getContainer=function(){return u},this.getSelectedItemIndex=function(){return w},this.getItems=function(){return v},this.getItemData=function(a){return v.length<a||void 0===v[a]?-1:v[a]},this.getSelectedItemData=function(){return this.getItemData(w)},this.build=function(){e()},this.init=function(){d()}},a.eacHandles=[],a.getHandle=function(b){return a.eacHandles[b]},a.inputHasId=function(a){return void 0!==$(a).attr("id")&&$(a).attr("id").length>0},a.assignRandomId=function(b){var c="";do c="eac-"+Math.floor(1e4*Math.random());while(0!==$("#"+c).length);elementId=a.consts.getValue("CONTAINER_ID")+c,$(b).attr("id",c)},a.setHandle=function(b,c){a.eacHandles[c]=b},a}(EasyAutocomplete||{});!function(a){a.fn.easyAutocomplete=function(b){return this.each(function(){var c=a(this),d=new EasyAutocomplete.main(c,b);EasyAutocomplete.inputHasId(c)||EasyAutocomplete.assignRandomId(c),d.init(),EasyAutocomplete.setHandle(d,c.attr("id"))})},a.fn.getSelectedItemIndex=function(){var b=a(this).attr("id");return void 0!==b?EasyAutocomplete.getHandle(b).getSelectedItemIndex():-1},a.fn.getItems=function(){var b=a(this).attr("id");return void 0!==b?EasyAutocomplete.getHandle(b).getItems():-1},a.fn.getItemData=function(b){var c=a(this).attr("id");return void 0!==c&&b>-1?EasyAutocomplete.getHandle(c).getItemData(b):-1},a.fn.getSelectedItemData=function(){var b=a(this).attr("id");return void 0!==b?EasyAutocomplete.getHandle(b).getSelectedItemData():-1}}(jQuery);


        });
    </script>
    <style>
        #goods-table th, td {
            border: 1px solid gray !important;
            padding: 2px;
            max-width: 50px;
            overflow: hidden;
        }
        #goods-table th, td>img{
            border: none;
            vertical-align: center;
        }
        *{
            font-family: 'PT Sans';
            font-size:14px;
        }

        table{
            width: 100%;
            border-color: #333;
            border-collapse: collapse;
        }

        table thead td,table tfoot td, .justText{
            padding: 5px;
            background: #ddd;
        }

        table tfoot td{
            background: #fdfdfd;
        }


        input{
            display: block;
            border: 0;
            line-height: 20px;
            width: 100%;
            font-size: 14px;
            padding: 5px;
        }

        input.error{
            background: red;
        }

        table td{
            padding: 0;
        }

        /*a{
            display: inline-block;
            margin-top: 15px;
        }*/

        .btn{
            max-height: 38px;
            padding: 10px;
            border: 1px solid #dbdedf;
            border-radius: 3px;
            vertical-align: middle;
            color: #2e3640;
            font-size: 14px;
            line-height: 14px;
            cursor: pointer;
            outline: none;
            z-index: 1;
            white-space: nowrap;
            text-decoration: none;
            border: 1px solid #4077d6;
            background: #4c8bf7;
            color: #fff;
            display: inline-block;
            cursor: pointer;
            margin-top: 25px;
            float: right;
            margin-right: 20px;
        }

        .add-btn{
            cursor: pointer;
            max-height: 38px;
            padding: 3px;
            border: 1px solid #dbdedf;
            border-radius: 3px;
            vertical-align: middle;
            color: #2e3640;
            white-space: nowrap;
            text-decoration: none;
            border: 1px solid #09d6d3;
            background: #09d6d3;
            color: #fff;
            display: inline-block;
            width: 100%;
            box-sizing: border-box;
            text-align: center;
        }

        .add-input{
            border: 1px solid #dbdedf;
            max-height: 38px;
            display: inline-block;
            width: 100%;
            box-sizing: border-box;
        }


        select{
            margin-bottom: 20px;
        }

        #quick{
            display: inline-block;
            width: auto;
        }

        a.disabled {
            pointer-events: none; /* делаем ссылку некликабельной */
            cursor: default;  /* устанавливаем курсор в виде стрелки */
            color: #999; /* цвет текста для нективной ссылки */
        }
        .del-row-table{
            cursor: pointer;
            color: red;
        }

        .report_c_nova{
            margin: 20px;
        }

        .iframe{
            position: absolute;
            top: 20px;
            left: 20px;
            z-index: 9999999;
            width: calc(100% - 40px);
            height: calc(100% - 40px);
        }

        .iframe iframe{
            width: 100%;
            height: 100%;
        }

        .iframe .close{
            position: absolute;
            top: 0px;
            right: 0px;
            color: #fff;
            font-family: Arial;
            padding: 10px;
            background: #333;
            border-radius: 3px;
            cursor: pointer;
            font-size: 14px;
        }

        .nova_preloader{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255,255,255,.5);
            z-index: 9999999999999;
        }

        .nova_preloader .text{
            font-size: 40px;
            color: #333;
            text-align: center;
            position: absolute;
            width: 100%;
            left: 0;
            top: 50%;
        }

        .easy-autocomplete{
            width: 100%!important;
        }
    </style>
</div>