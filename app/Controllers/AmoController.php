<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 07.09.2018
 * Time: 17:05
 */

namespace App\Controllers;


use App\Models\Amo\Account;

class AmoController
{
    public function getUsers($request, $response, $args)
    {
        $account = new Account();
        $users = $account->get('with=users')['users'];
        return $response->withJson($users);
    }
}