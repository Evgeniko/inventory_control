<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 07.09.2018
 * Time: 15:16
 */

namespace App\Controllers;


use App\Models\History;
use Illuminate\Database\Capsule\Manager as DB;

class HistoryController extends Controller
{
    public function getAll($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();

        $table = DB::table('history');
        if (isset($requestBody['type'])){
            if ($requestBody['type']){
                $table->where('type', $requestBody['type']);
            }
        }
        if (isset($requestBody['stock'])){
            if ($requestBody['stock']){
                $table->where('stock', $requestBody['stock']);
            }
        }
        if (isset($requestBody['item'])){
            if ($requestBody['item']){
                $table->where('item_id', $requestBody['item']);
            }
        }

        if (isset($requestBody['manager_id'])){
            if ($requestBody['manager_id']){
                $table->where('manager_id', $requestBody['manager_id']);
            }
        }

        $time = 0;
        if (isset($requestBody['period'])){
            if ($requestBody['period']){
                $period = $requestBody['period'];
                if ($period == 'day'){
                    $time = time() - 61*60*24;
                }
                if ($period == 'week'){
                    $time = time() - 61*60*24*7;
                }
                if ($period == 'month'){
                    $time = time() - 61*60*24*7*30;
                }
                if ($period == 'all'){
                    $time = 0;
                }
            }else{
                $time = time() - 61*60*24;;
            }
        }else{
            $time = time() - 61*60*24;
        }
        $table->where('date','>', $time);

        $history = $table->get();
        return $response->withJson($history);
    }
}