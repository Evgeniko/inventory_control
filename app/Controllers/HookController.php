<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 04.09.2018
 * Time: 16:33
 */

namespace App\Controllers;


use App\Models\Amo\Lead;
use App\Models\History;
use App\Models\Item;
use Interop\Container\ContainerInterface;

class HookController extends Controller
{
    public $amoSettings;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->amoSettings = $this->container->get('settings')['amo'];
    }

    public function leadDelete()
    {

    }


    //Хук на "Закрыто и не реализовано
    public function leadFailClose($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $leadId = $requestBody['leads']['status'][0]['id'];
        $Lead = new Lead();
        $lead = $Lead->get('id='.$leadId);
        file_put_contents(__DIR__.'/man.log', print_r($lead, 1), FILE_APPEND);
        $leadCount = count($lead);
        if ($leadCount == 1){
            $check = false;
            $manufacturedFieldId = $this->amoSettings['manufacturedFieldId'];
            $customFields = $lead[0]['custom_fields'];
            $managerId = $lead[0]['responsible_user_id'];
            foreach ($customFields as $field){
                $fieldId = $field['id'];
                if ($fieldId == $manufacturedFieldId){
                    $check = $field['values'][0]['value'];
                }
            }
            if ($check){
                //Изготовлен
                //Удалить
                $items = Item::where('lead_id', $leadId)->with('stock')->get();
                foreach ($items as $item) {
                    History::create([
                        'type' => WRITEOFF_TYPE,
                        'item_id' => $item->id,
                        'series' => $item->series,
                        'number' => $item->number,
                        'reestr' => $item->reestr,
                        'agent' => $item->agent,-
                        'manager_id' => $managerId,
                        'stock' => $item->stock->name,
                        'payment_price' => $item->payment_price,
                        'payment_source' => $item->payment_source,
                        'date' => time()
                    ]);
                    //$item->status = WRITEOFF_STATUS;
                    //$item->save();
                    $item->delete();
                }
            }else{
                //Не изготовлен
                Item::where('lead_id', $leadId)->update(['status' => ACTIVE_STATUS, 'lead_id' => NULL]);
            }
        }

    }

    //Хук на Успешно реализовано
    public function leadSuccessClose($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $leadId = $requestBody['leads']['status'][0]['id'];

        $Lead = new Lead();
        $lead = $Lead->get('id='.$leadId);
        $managerId = '';
        $leadCount = count($lead);
        if ($leadCount == 1){
            $managerId = $lead[0]['responsible_user_id'];
        }

        $items = Item::where('lead_id', $leadId)->with('stock')->get();
        foreach ($items as $item) {
            History::create([
                'type' => WRITEOFF_TYPE,
                'item_id' => $item->id,
                'series' => $item->series,
                'number' => $item->number,
                'reestr' => $item->reestr,
                'agent' => $item->agent,
                'manager_id' => $managerId,
                'stock' => $item->stock->name,
                'payment_price' => $item->payment_price,
                'payment_source' => $item->payment_source,
                'date' => time()
            ]);
            //$item->status = WRITEOFF_STATUS;
            //$item->save();
            $item->delete();
        }

    }

    //Хук на статус "Выдаачи номера"
    public function leadDelivery($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $leadId = $requestBody['leads']['status'][0]['id'];
        $Lead = new Lead();
        $lead = $Lead->get('id='.$leadId);
        $managerId = '';
        $leadCount = count($lead);
        if ($leadCount == 1){
            $managerId = $lead[0]['responsible_user_id'];
        }
        if ($leadId) {
            $items = Item::where('lead_id', $leadId)->with('stock')->get();
            foreach ($items as $item) {
                History::create([
                    'type' => WRITEOFF_TYPE,
                    'item_id' => $item->id,
                    'series' => $item->series,
                    'number' => $item->number,
                    'reestr' => $item->reestr,
                    'agent' => $item->agent,
                    'manager_id' => $managerId,
                    'stock' => $item->stock->name,
                    'payment_price' => $item->payment_price,
                    'payment_source' => $item->payment_source,
                    'date' => time()
                ]);
                $item->status = WRITEOFF_STATUS;
                $item->save();
            }
        }
    }
}