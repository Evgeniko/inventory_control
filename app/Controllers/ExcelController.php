<?php

namespace App\Controllers;

use App\Models\History;
use App\Models\Item;
use App\Models\Stock;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Illuminate\Database\Capsule\Manager as DB;

class ExcelController extends Controller
{

    public function import($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $stockId = $requestBody['stock_id'];
        $stock = Stock::find($stockId);
        $file = $_FILES['items_excel_file'];
        $file = $file['tmp_name'];
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($file);
        try{
            DB::beginTransaction();
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key == 1){
                        continue;
                    }
                    $Npp = $row[0];
                    $series = $row[1];
                    $number = $row[2];
                    $reestr = $row[3];
                    $status = $row[4];
                    $date = $row[5];
                    $agent = $row[6];
                    if (!$Npp && !$series && !$number ){
                        break;
                    }else{
                        $item = Item::create([
                            'stock_id' => $stockId,
                            'series' => $series,
                            'number' => $number,
                            'reestr' => $reestr,
                            'agent' => $agent,
                            'status' => ACTIVE_STATUS,
                            'agent' => $agent
                        ]);
                        History::create([
                            'type' => ADD_TYPE,
                            'item_id' => $item->id,
                            'series' => $series,
                            'number' => $number,
                            'reestr' => $reestr,
                            'agent' => $agent,
                            'manager_id' => '',
                            'stock' => $stock->name,
                            'payment_price' => '',
                            'payment_source' => '',
                            'date' => time()
                        ]);
                    }
                }
                break;
            }
            DB::commit();
        }catch (\PDOException $e){
            DB::rollback();
        }

        $reader->close();
        return $response->getBody()->write(1);
    }

    private function removeBom($str)
    {
        if(substr($str, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
            $str = substr($str, 3);
        }
        return $str;
    }

}

