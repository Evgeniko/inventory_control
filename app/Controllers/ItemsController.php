<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 03.09.2018
 * Time: 10:53
 */

namespace App\Controllers;

use App\Models\Amo\Lead;
use App\Models\History;
use App\Models\Item;
use App\Models\Stock;

class ItemsController extends Controller
{
    public function add($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $item = Item::create($requestBody);
        $stockId = $requestBody['stock_id'];

        History::create([
            'type' => ADD_TYPE,
            'item_id' => $item->id,
            'series' => $requestBody['series'],
            'number' => $requestBody['number'],
            'reestr' => $requestBody['reestr'],
            'agent' => '',
            'manager_id' => '',
            'stock' => Stock::find($stockId)->name,
            'payment_price' => $requestBody['payment_price'],
            'payment_source' => $requestBody['payment_source'],
            'date' => time()
        ]);
        return $response->getBody()->write(1);

    }

    public function delete($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $ids = $requestBody['ids'];
        $items = Item::whereIn('id', $ids)->with('stock')->get();

        foreach ($items as $item){
            History::create([
                'type' => DELETE_TYPE,
                'item_id' => $item->id,
                'series' => $item->series,
                'number' => $item->number,
                'reestr' => $item->reestr,
                'agent' => $item->agent,
                'manager_id' => '',
                'stock' => $item->stock->name,
                'payment_price' => $item->payment_price,
                'payment_source' => $item->payment_source,
                'date' => time()
            ]);
            $item->delete();
        }

        return $response->getBody()->write(1);
    }

    public function move($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $ids = $requestBody['ids'];
        $stockId = $requestBody['stock_id'];
        $stock = Stock::find($stockId);
        $stockName = $stock->name;
        $items = Item::whereIn('id', $ids)->with('stock')->get();
        //Item::whereIn('id', $ids)->update(['stock_id' => $stockId]);
        foreach ($items as $item) {
            History::create([
                'type' => MOVE_TYPE,
                'item_id' => $item->id,
                'series' => $item->series,
                'number' => $item->number,
                'reestr' => $item->reestr,
                'agent' => $item->agent,
                'manager_id' => '',
                'stock' => $stockName,
                'payment_price' => $item->payment_price,
                'payment_source' => $item->payment_source,
                'date' => time()
            ]);
            $item->stock_id = $stock->id;
            $item->save();
        }
        return $response->getBody()->write(1);
    }

    public function reserved ($request, $response, $args)
    {
        //Сначала unreserve всех товаров
        $requestBody = $request->getParsedBody();

        $leadId = $requestBody['lead_id'];
        $Lead = new Lead();
        $lead = $Lead->get('id='.$leadId);
        $managerId = '';
        $leadCount = count($lead);
        if ($leadCount == 1){
            $managerId = $lead[0]['responsible_user_id'];
        }
        //Обнуление статуса и откреп от сделки
        $this->unlinkLead($leadId);
        //Установка статуса нужным элементам и закрепление к сделкам
        if (array_key_exists('ids', $requestBody)){
            $ids = $requestBody['ids'];
            $items = Item::whereIn('id', $ids)->with('stock')->get();
            foreach ($items as $item) {
                History::create([
                    'type' => RESERVE_TYPE,
                    'item_id' => $item->id,
                    'series' => $item->series,
                    'number' => $item->number,
                    'reestr' => $item->reestr,
                    'agent' => $item->agent,
                    'manager_id' => $managerId,
                    'stock' => $item->stock->name,
                    'payment_price' => $item->payment_price,
                    'payment_source' => $item->payment_source,
                    'date' => time()
                ]);
                $item->status = RESERVE_STATUS;
                $item->lead_id = $leadId;
                $item->save();
            }
            //Item::whereIn('id', $ids)->update(['status' => RESERVE_STATUS, 'lead_id' => $leadId]);
        }

        return $response->getBody()->write(1);
    }

    public function getReserved($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $leadId = $args['id'];
        $items = Item::where('lead_id', $leadId)->get();

        return $response->withJson($items);

    }

    public function getAll($request, $response, $args)
    {
        $items = Item::all();

        return $response->withJson($items);
    }

    private function unlinkLead($leadId)
    {
        Item::where('lead_id', $leadId)->update(['status' => ACTIVE_STATUS, 'lead_id' => NULL]);
        return true;
    }


}