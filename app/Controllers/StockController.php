<?php

namespace App\Controllers;

use App\Models\Item;
use App\Models\Stock;
use App\Models\Json\Stock as StockJson;
use Interop\Container\ContainerInterface;

class StockController extends Controller
{
    protected $stockModel;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->stockModel = new StockJson();
    }

    //Добавление нового склада
    public function add($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $name = $requestBody['name'];
        Stock::create(['name' => $name]);

        return $response->getBody()->write(1);
    }

    //Удаление склада
    public function delete($request, $response, $args)
    {
        $requestBody = $request->getParsedBody();
        $id = $requestBody['id'];
        Stock::find($id)->items()->delete();
        Stock::destroy($id);

        return $response->getBody()->write(1);
    }


    //Получение всех складов
    public function all($request, $response, $args)
    {
        $result = Stock::all();
        return $response->withJson($result);
    }

    //Получение всех складов с их продуктами
    protected function allWithItems()
    {
        return Stock::with('items')->get();
    }

    //Получение скалада и его сделок по id (либо получение всех если не выбран склад)
    public function getByIdWithItems($request, $response, $args)
    {
        $id = $args['id'];
        if ($id == 0){
            $result = $this->allWithItems();
        }else{
            $stock = Stock::find($id);
            $items = $stock->items;
            $stock['items'] = $items;
            $result[] = $stock;
            //$result = $stock->items;
        }
        return $response->withJson($result);
    }

    public function getAllWithItems($request, $response, $args)
    {
        $result = $this->allWithItems();

        return $response->withJson($result);
    }

    public function getAllWithItemsActive($request, $response, $args)
    {
        $result = Stock::with(['items' => function($query){
            $query->where('status', ACTIVE_STATUS);
        }])->get();
        return $response->withJson($result);
    }
}