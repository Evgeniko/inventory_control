<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 03.09.2018
 * Time: 10:50
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $guarded = ['id'];

    public function stock()
    {
        return $this->belongsTo('App\Models\Stock');
    }
}