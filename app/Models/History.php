<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 07.09.2018
 * Time: 10:28
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';
    protected $guarded = ['id'];
}