<?php

namespace App\Models\Json;

class Model
{
    protected $config;
    protected $storagePath;

    public function __construct()
    {
        $this->config = include_once(ROOT . '/config/storage.php');
        $this->storagePath = $this->config['storagePath'];
    }
}