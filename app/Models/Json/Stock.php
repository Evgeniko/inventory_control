<?php

namespace App\Models\Json;


class Stock extends Model
{
    public $folder = 'stocks/';

    public function __construct()
    {
        parent::__construct();
        $this->storagePath = $this->storagePath.$this->folder;
    }

    public function create($name)
    {
        //Нужна проверка на существование
        $filePath = $this->storagePath.$name.'.json';
        $data = [
            'name' => $name,
            'items' => []
        ];
        $data = json_encode($data);
        file_put_contents($filePath, $data);
    }

    public function delete($name)
    {
        $filePath = $this->storagePath.$name.'.json';
        unlink($filePath);
    }

    public function update()
    {
        //Переименование склада
    }

    public function getAll()
    {
        $dir = $this->storagePath;
        $folder = scandir($dir);
        $stocks = [
            'items' => [],
            'names' => []
            ];
        foreach ($folder as $file){
            if (strripos($file, '.json') !== false){
                $fileContent = json_decode(file_get_contents($dir.$file), 1);
                $items = $fileContent['items'];
                $stocks['items'] = array_merge($stocks['items'], $items);
                $stocks['names'][] = $fileContent['name'];
            }
        }
        return json_encode($stocks);
    }

    public function getByName($name)
    {
        $filePath = $this->storagePath.$name.'.json';
        $data = file_get_contents($filePath);
        return $data;
    }
    
    public function getAllNames()
    {
        $names = [];
        $dir = $this->storagePath;
        $folder = scandir($dir);
        foreach ($folder as $file){
            if (strripos($file, '.json') !== false){;
                $name = str_replace('.json', '', $file);
                $names[] = $name;
            }
        }
        return $names;
    }

    public function addItem($item)
    {
        $stockName = $item['stock_name'];
        $filePath = $this->storagePath.$stockName.'.json';
        $data = json_decode(file_get_contents($filePath), 1);
        $data['items'][] = $item;
        $data = json_encode($data);
        file_put_contents($filePath, $data);
    }

    public function test()
    {
        $stockName = 'Какой-0то супер склад ;))№';
        $items = [];
        for ($i=0;$i<5000;$i++){
            $items[] = [
                'stock_name' => rand(),
                'series' => rand(),
                'number' => rand(),
                'reestr' => rand(),
                'status' => rand(),
                'payment_price' => rand(),
                'payment_source' => rand()
            ];
        }
        $filePath = $this->storagePath.$stockName.'.json';
        $data = json_decode(file_get_contents($filePath), 1);
        $data['items'] = $items;
        $data = json_encode($data);
        file_put_contents($filePath, $data);
    }
}