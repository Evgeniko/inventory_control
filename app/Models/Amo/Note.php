<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 28.08.2018
 * Time: 17:49
 */

namespace App\Models\Amo;


class Note extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'notes';
    }
}