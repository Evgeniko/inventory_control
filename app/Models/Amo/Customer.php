<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 28.08.2018
 * Time: 17:48
 */

namespace App\Models\Amo;


class Customer extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'customers';
    }
}