<?php

namespace App\Models\Amo;


class Lead extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'leads';
    }

    public function get($params = '')
    {
        $result = $this->compound($this->entity, $params);
        return $result;
    }


    //Получение сделок с сортировкой(фильтрацией) по именам сделок
    //Отбрасываются сделки с одинаковыми именами
    public function getSortedByName($params = '')
    {
        $result = [];
        $leads = $this->get($params);
        foreach ($leads as $lead) {
            $name = $lead['name'];
            $result[$name] = $lead;
        }
        return $result;
    }

    public function add()
    {
        
    }

    public function update()
    {
        
    }
}