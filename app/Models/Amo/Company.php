<?php

namespace App\Models\Amo;

class Company extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'companies';
    }

    public function get($params = '')
    {
        $result = $this->compound($params);
        return $result;
    }

    public function add()
    {

    }

    public function update()
    {

    }
}