<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 28.08.2018
 * Time: 17:47
 */

namespace App\Models\Amo;


class Contact extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'contacts';
    }
}