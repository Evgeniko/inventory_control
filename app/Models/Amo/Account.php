<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 28.08.2018
 * Time: 17:47
 */

namespace App\Models\Amo;


class Account extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'account';
    }

    public function get($params = '')
    {
        $result = $this->getQuery($this->entity, $params);
        return $result;
    }


    protected function getQuery($entity, $params = '')
    {
        $subdomain = $this->config['subdomain']; #Наш аккаунт - поддомен
        /* Формируем ссылку для запроса */
        $link="https://$subdomain.amocrm.ru/api/v2/$entity?$params";
        //var_dump($link);
        $curl=curl_init();
        /* Устанавливаем необходимые опции для сеанса cURL */
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,$this->cookiePath); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_COOKIEJAR,$this->cookiePath); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        /* Выполняем запрос к серверу. */
        $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);
        /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            /* Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке */
            if($code!=200 && $code!=204) {
                throw new \Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
            }
        }
        catch(\Exception $E)
        {
            die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
        }
        /*
         Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         нам придётся перевести ответ в формат, понятный PHP
         */
        $Response=json_decode($out,true);;
        return $Response['_embedded'];
    }
}