<?php

namespace App\Models\Amo;

class CatalogElement extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'catalog_elements';
    }

    public function get($params = '')
    {
        $result = [];
        $offset = 0;
        $limit = 500;
        do {
            $count = 0;
            $query = '';
            if ($params){
                $query .= $params."&limit_rows=$limit&limit_offset=$offset";
            }else{
                $query = "&limit_rows=$limit&limit_offset=$offset";
            }
            $part = $this->getQuery('catalog_elements', $query);
            if($part){
                $count = count($part);
                $result = array_merge($result, $part);
            }
            $offset+=500;
        } while ( $count == 500);
        return $result;
    }
}