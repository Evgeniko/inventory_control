<?php

namespace App\Models\Amo;

class Model
{
    public $auth;
    public $error;
    public $errorCode;
    public $config;
    public $cookiePath;
    protected $entity;

    public function __construct()
    {
        $this->config = include(ROOT . '/config/amo.php');
        $this->cookiePath = ROOT.'/storage/amo/cookie.txt';
        $this->auth();
    }

    /**
     * @return mixed
     */
    public function auth()
    {
        $user=array(
            'USER_LOGIN' => $this->config['USER_LOGIN'], #Ваш логин (электронная почта)
            'USER_HASH' => $this->config['USER_HASH'] #Хэш для доступа к API (смотрите в профиле пользователя)
        );
        $subdomain = $this->config['subdomain']; #Наш аккаунт - поддомен
        #Формируем ссылку для запроса
        $link='https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
        /* Нам необходимо инициировать запрос к серверу. Воспользуемся библиотекой cURL (поставляется в составе PHP). Вы также
        можете
        использовать и кроссплатформенную программу cURL, если вы не программируете на PHP. */
        $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
        #Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($user));
        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE, $this->cookiePath);
        curl_setopt($curl,CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
        curl_close($curl); #Завершаем сеанс cURL
        /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if($code!=200 && $code!=204)
                throw new \Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
        }
        catch(\Exception $E)
        {
            die();
        }

        if($code!=200 && $code!=204){
            $error = isset($errors[$code]) ? $errors[$code] : 'Undescribed error';
            $this->error = 'Ошибка: '.$error.PHP_EOL.'Код ошибки: '.$code;
            $this->errorCode = $code;
        }
        /*
         Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         нам придётся перевести ответ в формат, понятный PHP
         */
        $Response=json_decode($out,true);
        $Response=$Response['response'];
        if(isset($Response['auth'])){
            $this->auth = true;
        } else{
            $this->auth = false;
        }
    }

    //Общий метод отправки GET-запроса в amo
    protected function getQuery($entity, $params = '')
    {
        $subdomain = $this->config['subdomain']; #Наш аккаунт - поддомен
        /* Формируем ссылку для запроса */
        $link="https://$subdomain.amocrm.ru/api/v2/$entity?$params";
        //var_dump($link);
        $curl=curl_init();
        /* Устанавливаем необходимые опции для сеанса cURL */
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,$this->cookiePath); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_COOKIEJAR,$this->cookiePath); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        /* Выполняем запрос к серверу. */
        $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);
        /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code=(int)$code;
        $errors=array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            /* Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке */
            if($code!=200 && $code!=204) {
                throw new \Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
            }
        }
        catch(\Exception $E)
        {
            die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
        }
        /*
         Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         нам придётся перевести ответ в формат, понятный PHP
         */
        $Response=json_decode($out,true);
        $Response=$Response['_embedded']['items'];
        return $Response;
    }

    //Общий метод отправки POST-запроса в amo
    protected function postQuery($entity, $data)
    {

    }

    //Получение всех даных с отступами
    public function compound($entity, $params = '')
    {
        $result = [];
        $offset = 0;
        $limit = 500;
        do {
            $count = 0;
            $query = '';
            if ($params){
                $query .= $params."&limit_rows=$limit&limit_offset=$offset";
            }else{
                $query = "&limit_rows=$limit&limit_offset=$offset";
            }
            $part = $this->getQuery($entity, $query);
            if($part){
                $count = count($part);
                $result = array_merge($result, $part);
            }
            $offset+=500;
        } while ( $count == 500);
        return $result;
    }

    public function postCompound($data)
    {

    }

}