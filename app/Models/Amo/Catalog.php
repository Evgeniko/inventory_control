<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 29.08.2018
 * Time: 12:02
 */

namespace App\Models\Amo;


class Catalog extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'catalogs';
    }
}