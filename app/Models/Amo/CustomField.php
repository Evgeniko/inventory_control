<?php
/**
 * Created by PhpStorm.
 * User: Евгений
 * Date: 28.08.2018
 * Time: 17:53
 */

namespace App\Models\Amo;


class CustomField extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->entity = 'fields';
    }
}