<?php


$app->get('/', 'ExampleController:index');


$app->get('/catalogElements/getByCatalogIdWithJson/{id}', 'CatalogElementsController:getByCatalogIdWithJson');

$app->get('/stock/getAll', 'StockController:getAll');
$app->get('/stock/getById/{id:[0-9]+}', 'StockController:getByIdWithItems');
$app->get('/stock/all', 'StockController:all');
$app->get('/stock/allWithItems', 'StockController:getAllWithItems');
$app->get('/stock/allWithItemsActive', 'StockController:getAllWithItemsActive');
$app->post('/stock/add', 'StockController:add');
$app->post('/stock/delete', 'StockController:delete');

$app->post('/item/add', 'ItemsController:add');
$app->post('/items/delete', 'ItemsController:delete');
$app->post('/items/move', 'ItemsController:move');
$app->post('/items/reserved', 'ItemsController:reserved');
$app->get('/items/getReserved/{id:[0-9]+}', 'ItemsController:getreserved');
$app->post('/items/unreserved', 'ItemsController:unreserved');
$app->post('/items/writeoff', 'ItemsController:writeoff');
$app->get('/items/get/all', 'ItemsController:getAll');

$app->post('/history/all', 'HistoryController:getAll');

$app->post('/hook/lead/delivery', 'HookController:leadDelivery');
$app->post('/hook/lead/failClose', 'HookController:leadFailClose');
$app->post('/hook/lead/successClose', 'HookController:leadSuccessClose');
$app->post('/hook/lead/delete', 'HookController:leadDelete');


$app->get('/amo/users', 'AmoController:getUsers');

$app->post('/excel/import', 'ExcelController:import');


//$app->post('/excel/import', 'ExcelController:import');

//Возвращает конфигурацию в json виде
$app->get('/amo/config', function ($request, $response){
    $amoconfig = require_once (ROOT.'/config/amo.php');
    //$amoconfig = json_encode($amoconfig);
    return $response->withJson($amoconfig, 201);
});