<?php
header('Access-Control-Allow-Origin: *');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
session_start();
define('ROOT',__DIR__);
define("ACTIVE_STATUS", 'active');
define("RESERVE_STATUS", 'reserve');
define("WRITEOFF_STATUS", 'writeoff');
define("ADD_TYPE", 'add');
define("MOVE_TYPE", 'move');
define("RESERVE_TYPE", 'reserved');
define("WRITEOFF_TYPE", 'writeoff');
define("DELETE_TYPE", 'delete');
require_once (ROOT.'/bootstrap/app.php');
require_once (ROOT.'/routes/routes.php');


$app->run();

