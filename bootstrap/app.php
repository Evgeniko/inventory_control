<?php

/**
 * Файл предварительной конфигурации
 */

require __DIR__.'/../vendor/autoload.php';
$amoConfig = __DIR__.'/../config/amo.php';
$dbConfig = require (__DIR__.'/../config/db.php');
$amo = require(__DIR__.'/../config/amo.php');

$app = new \Slim\App([

   'settings' => [
       'displayErrorDetails' => true,
       'amo' => $amo,
       'db' => $dbConfig,
   ]

]);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule){
    return $capsule;
};


//Определение контроллера в контейнер
$container['ExampleController'] = function ($container){
    return new \App\Controllers\ExampleController($container);
};


$container['ExcelController'] = function ($container){
    return new \App\Controllers\ExcelController($container);
};

$container['CatalogElementsController'] = function ($container){
    return new \App\Controllers\CatalogElementsController($container);
};

$container['StockController'] = function ($container){
    return new \App\Controllers\StockController($container);
};

$container['ItemsController'] = function ($container){
    return new \App\Controllers\ItemsController($container);
};

$container['HookController'] = function ($container){
    return new \App\Controllers\HookController($container);
};

$container['HistoryController'] = function ($container){
    return new \App\Controllers\HistoryController($container);
};

$container['AmoController'] = function ($container){
    return new \App\Controllers\AmoController($container);
};