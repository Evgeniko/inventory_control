<?php
    header('Access-Control-Allow-Origin: *');
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    $config = require_once '../../config/amo.php';
    $configJSON = json_encode($config);
    $URI = $config['siteURI'];
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/dataTables/datatables.min.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/sweetalert.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/style.css">
</head>
<body>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h2 id="stock-name-label" class="text-center" attr-id="">Все склады</h2>
            </div>
            <div class="ibox-content">

                <!--<div class="table-responsive">-->
                <form action="<?php echo $URI ?>excel/import" method="POST" enctype="multipart/form-data">
                    <input name="items_excel_file" id="file_download_items" type="file" style="display: none" accept=".xlsx">
                    <input id="submit-file" type="submit" style="display: none">
                </form>
                <a id="add-item-modal" data-toggle="modal" class="btn btn-primary" href="#modal-form">Добавить</a>
                <a id="add_items_excel" class="btn btn-success" href="#">Загрузка Excel</a>
                <a attr-action="delete" class="btn btn-danger selected_action" href="#">Удалить</a>
                <a id="action_move_btn" attr-action="move" class="btn btn-success selected_action" href="#">Переместить</a>
                <div id="modal-form" class="modal fade" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <p>Добавить элемент на склад</p>
                                    <form id="add-item-form" class="unchecked" role="form" action="<?php echo $URI ?>item/add" method="post">
                                        <div class="form-group"><label>Склад</label> <select class="form-control m-b" id="stock_name_select" name="stock_id">
                                                <option disabled value="false" selected>Выберите склад для добавления</option>
                                            </select></div>
                                        <div class="form-group"><label>Серия</label> <input type="text" name="series" placeholder="Серия" class="form-control"></div>
                                        <div class="form-group"><label>Номер</label> <input type="text" name="number" placeholder="Номер" class="form-control"></div>
                                        <div class="form-group"><label>Реестр</label> <input type="text" name="reestr" placeholder="Реестр" class="form-control"></div>
                                        <div class="form-group"><label>Статус</label> <input type="text" name="status" placeholder="Статус" class="form-control"></div>
                                        <div class="form-group"><label>Цена закупки</label> <input type="text" name="payment_price" placeholder="Цена закупки" class="form-control"></div>
                                        <div class="form-group"><label>Источник закупки</label> <input type="text" name="payment_source" placeholder="Источник закупки" class="form-control"></div>
                                    </form>
                                        <div>
                                            <button id="add_item_btn" class="btn btn-sm btn-primary pull-right m-t-n-xs" data-dismiss="modal" type="submit"><strong>Добавить</strong></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                        <thead>
                            <tr role="row">
                                <th></th>
                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Склад: activate to sort column descending">Склад</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Серия</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Номер</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Реестр</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Статус</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Цена закупки</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Источник закупки</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="gradeA even" role="row">
                            </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">Склад</th>
                            <th rowspan="1" colspan="1">Серия</th>
                            <th rowspan="1" colspan="1">Номер</th>
                            <th rowspan="1" colspan="1">Реестр</th>
                            <th rowspan="1" colspan="1">Статус</th>
                            <th rowspan="1" colspan="1">Цена закупки</th>
                            <th rowspan="1" colspan="1">Источник закупки</th>
                        </tr>
                        </tfoot>
                    </table>
                <!--</div>-->
            </div>
        </div>
    </div>
</div>
    <script src="<?php echo $URI ?>resources/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $URI ?>resources/js/underscore-min.js"></script>
    <script src="<?php echo $URI ?>resources/js/bootstrap.min.js"></script>
    <script src="<?php echo $URI ?>resources/js/sweetalert.min.js"></script>
    <script src="<?php echo $URI ?>resources/js/dataTables/datatables.min.js"></script>
    <script src="<?php echo $URI ?>resources/js/dataTables.checkboxes.min.js"></script>
    <script>
        $(document).ready(function(){
            var configJSON = '<?php echo $configJSON?>';
            var WIDGETCONFIG = JSON.parse(configJSON);
            var URI = WIDGETCONFIG.siteURI;
            var PreloaderHelper = {
                show: function () {
                    $('body').append('<div class="nova_preloader">' +
                        '<div class="container-preloader-evg">' +
                        '<div class="circular-container">' +
                        '<div class="circle circular-loader1">' +
                        '<div class="circle circular-loader2"></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                },
                hide: function () {
                    $('.nova_preloader').remove();
                }
            };
            PreloaderHelper.show();
            if (window.addEventListener) {
                window.addEventListener("message", listener);
            } else {
                // IE8
                window.attachEvent("onmessage", listener);
            }
            function listener(event) {
                var current_stock = JSON.parse(event.data);
                console.log(current_stock);
                if (current_stock.id == 0){
                    $('#stock-name-label').html("Все склады");
                    $('#add_items_excel').remove();
                }else {
                    $('#stock-name-label').html(current_stock.name)
                    $('#stock-name-label').attr('attr-id', current_stock.id);


                    $('#add_items_excel').click(function () {
                        console.log('sss');
                        $('#file_download_items').click();
                        $('#file_download_items').on('change', function () {
                            $('#add_items_excel').html('Загрузка файла...');
                            var fd = new FormData();
                            fd.append( 'items_excel_file', $('#file_download_items')[0].files[0]);
                            fd.append('stock_id', current_stock.id);
                            $.ajax({
                                type: "POST",
                                url: URI+'excel/import',
                                data: fd,
                                processData: false,
                                contentType: false,
                                success: function(res){
                                    console.log(res);
                                    if (res == 1){
                                        $('#add_items_excel').html('Файл успешно загружен');
                                        $('#add_items_excel').css('background-color', 'green');

                                        swal("Загружен", "Файл был успешно загружен", "success");
                                    }
                                },
                                error: function () {
                                    $('#add_items_excel').html('Ошибка загрузки файла');
                                    $('#add_items_excel').css('background-color', 'red');
                                }
                            });
                        });
                    });

                }
                $.get(URI+'stock/all', {}, function(stocks){
                    console.log(stocks);
                    var ALL_STOCKS = stocks;
                    _.each(stocks, function (stock, i) {
                        var selected = false;
                        if (stock.id == current_stock.id){
                            selected = true;
                        }
                        $('#stock_name_select').append($('<option>', {
                            value: stock.id,
                            text : stock.name,
                            selected: selected
                        }));
                    });
                    $.get(URI+'stock/getById/'+current_stock.id, {}, function (data) {
                        var tableData = [];
                        //data = JSON.parse(data);
                        var stocks = data;
                        _.each(stocks, function (stock, i) {
                            var stock_name = stock.name;
                            _.each(stock.items, function (item, i) {
                                var status = 'Активен';
                                var status_code  = item.status;
                                if (status_code == 'reserve'){
                                    status = 'Зарезервирован';
                                }else if (status_code == 'writeoff'){
                                    status = 'Списан';
                                }
                                tableData.push([
                                    item.id,
                                    stock_name,
                                    item.series,
                                    item.number,
                                    item.reestr,
                                    status,
                                    item.payment_price,
                                    item.payment_source
                                ]);
                            });
                        });
                        console.log(tableData);
                        var table = $('.dataTables-example').DataTable({
                            data: tableData,
                            pageLength: 25,
                            responsive: true,
                            dom: '<"html5buttons"B>lTfgitp',
                            'columnDefs': [
                                {
                                    'targets': 0,
                                    'checkboxes': {
                                        'selectRow': true
                                    }
                                }
                            ],
                            'select': {
                                'style': 'multi'
                            },
                            'order': [[1, 'asc']],
                            buttons: [
                                {extend: 'copy'},
                                {extend: 'csv'},
                                {extend: 'excel', title: 'ExampleFile'},
                                {extend: 'pdf', title: 'ExampleFile'},

                                {
                                    extend: 'print',
                                    customize: function (win) {
                                        $(win.document.body).addClass('white-bg');
                                        $(win.document.body).css('font-size', '10px');

                                        $(win.document.body).find('table')
                                            .addClass('compact')
                                            .css('font-size', 'inherit');
                                    }
                                }
                            ],
                            "rowCallback": function( row, data, index ) {
                                if ( data[5] == "Зарезервирован" ) {
                                    $('td', row).css('background-color', '#86c1ff');
                                }
                                if ( data[5] == "Списан" ) {
                                    $('td', row).css('background-color', '#ffb4b4');
                                }
                            }

                        });
                        PreloaderHelper.hide();


                        //Обработчики на удаление и перемещение выделенных элементов в таблице
                        $('.selected_action').click(function (e) {
                            e.preventDefault();
                            var selectedItems = [];
                            var action = $(this).attr('attr-action'); //delete, move
                            var rows_selected = table.column(0).checkboxes.selected();
                            $.each(rows_selected, function(index, rowId){
                                selectedItems.push(rowId);
                            });

                            if (!selectedItems.length){
                                swal({
                                    title: "Ошибка",
                                    text: "Не отмечены элементы",
                                    icon: "error",
                                    button: "OK",
                                });
                                return false;
                            }

                            if(action == 'delete'){
                                swal({
                                    title: "Подтверждение",
                                    text: "Вы действительно хотите удалить выбранные элементы?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Да, удалить",
                                    cancelButtonText: "Нет, вернуться",
                                    closeOnConfirm: false,
                                    closeOnCancel: false
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        $.post(URI+'items/delete', {ids: selectedItems}, function (res) {
                                            if (res == 1){
                                                swal("Удалено", "Выбранные вами элементы были успешно удалены", "success");
                                            }else {
                                                swal("Ошибка", "Произошла ошибка при удалении", "error");
                                            }
                                        });
                                    } else {
                                        swal("Отмена", "Удаление было отмененно", "error");
                                    }
                                });

                            }

                            if (action == 'move'){
                                if($('#move-confirm').length == 0){
                                    $('#action_move_btn').after(
                                        '<form role="form" class="col-sm-2 form-inline">' +
                                            '<div class="form-group">' +
                                                '<select class="form-control m-b" id="move_to_stock" name="stock_id">' +
                                                    '<option disabled="" value="false" selected>Выберите склад для перемещения</option>'+
                                                '</select>' +
                                            '</div>'+
                                            '<div class="form-group">' +
                                                '<a id="move-confirm" attr-action="move" class="btn btn-success selected_action" href="#">Переместить</a>' +
                                            '</div>'+
                                        '</form>'
                                    );

                                    _.each(ALL_STOCKS, function (stock, i) {
                                        $('#move_to_stock').append($('<option>', {
                                            value: stock.id,
                                            text : stock.name,
                                        }));
                                    });

                                    $('#move-confirm').click(function (e) {
                                        e.preventDefault();
                                        var stock_id = $('#move_to_stock').val();
                                        $.post(URI+'items/move', {stock_id: stock_id, ids: selectedItems} , function (res) {
                                            if (res == 1){
                                                swal("Перемещено", "Выбранные вами элементы были успешно перемещены", "success");
                                            } else{
                                                swal("Ошибка", "Произошла ошибка при перемещении", "success");
                                            }
                                        });
                                    });
                                }
                            }
                            console.log(selectedItems);
                        });



                    });
                });
            }

            //Добавление элемента на склад
            //Проверка формы "добавления нового продукта" на обязательные поля
            $('#add_item_btn').click(function() {
                var stock_name = $('[name="stock_id"]').val();
                if (!stock_name){
                    swal("Упс", "Не выбран склад для добавления", "error");
                    return false;
                }
                var $form = $('#add-item-form');
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize()
                }).done(function(data) {
                    if (data == 1){
                        swal("Добавлено", "Новый элемент был успешно добавлен", "success");
                    }else{
                        swal("Упс", "Что-то пошло не так", "error");
                    }
                }).fail(function() {
                    swal("Упс", "Что-то пошло не так", "error");
                });
            });
        });
        
    </script>
    <style>
    .nova_preloader{
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(255,255,255,.5);
        z-index: 9999999;
    }
    .container-preloader-evg{
        text-align: center;
        color: #fff;
        text-align: center;
        position: absolute;
        width: 100%;
        left: 0;
        top: 50%;
    }
    .circular-container{
        width:10%;
        margin: 0 auto;
    }

    .circle{
        border: 5px solid transparent;
        border-radius: 50%;
    }

    .circular-loader1{
        width: 300px;
        height: 300px;
        display: table;
        padding: 1px;
        border-top: 5px solid #006495;
        border-bottom: 5px solid #006495;
        animation: circular_loader1 linear 2s infinite;
    }

    .circular-loader2{
        width: 300px;
        height: 30px;
        display: table-cell;
        border-right: 5px solid #3498db;
        border-left: 5px solid #3498db;
        animation: circular_loader2 linear 2s infinite;
    }

    @keyframes circular_loader1 {
        0% {
            transform: rotate(0deg);
        }
        50% {
            transform: rotate(-90deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes circular_loader2 {
        0% {
            transform: rotate(0deg);
        }
        50% {
            transform: rotate(-180deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
</body>
</html>