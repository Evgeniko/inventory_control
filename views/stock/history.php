<?php
header('Access-Control-Allow-Origin: *');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
$config = require_once '../../config/amo.php';
$configJSON = json_encode($config);
$URI = $config['siteURI'];
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/sweetalert.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo $URI ?>resources/css/style.css">
</head>
<body>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">История</h3>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-1 m-b-xs">
                        <select id="action_type_filter" class="input-sm form-control input-s-sm inline">
                            <option value=""  selected>Все операции</option>
                        </select>
                    </div>
                    <div class="col-sm-2 m-b-xs">
                        <select id="stock_filter" class="input-sm form-control input-s-sm inline">
                            <option value=""  selected>Все склады</option>
                        </select>
                    </div>
                    <div class="col-sm-2 m-b-xs">
                        <select id="item_filter" class="input-sm form-control input-s-sm inline">
                            <option value=""  selected>Все материалы</option>
                        </select>
                    </div>
                    <div class="col-sm-1 m-b-xs">
                        <select id="manager_id_filter" class="input-sm form-control input-s-sm inline">
                            <option value=""  selected>Все менеджеры</option>
                        </select>
                    </div>
                    <div class="col-sm-2 m-b-xs">
                        <div data-toggle="buttons" class="btn-group">
                            <label class="btn btn-sm btn-white active"> <input type="radio" id="option1" name="period_filter" value="day"> День </label>
                            <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="period_filter" value="week"> Неделя </label>
                            <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period_filter" value="month"> Месяц </label>
                            <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="period_filter" value="all"> Всё время </label>
                        </div>
                    </div>
                    <div id="confirm_history_filter" class="text-center col-sm-1">
                        <a class="btn btn-primary" href="#">Применить фильтр</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="history-table" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Операция </th>
                            <th>Менеджер</th>
                            <th>Склад</th>
                            <th>Серия </th>
                            <th>Номер</th>
                            <th>Агент</th>
                            <th>Закупочная</th>
                            <th>Источник</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo $URI ?>resources/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $URI ?>resources/js/underscore-min.js"></script>
<script src="<?php echo $URI ?>resources/js/bootstrap.min.js"></script>
<script src="<?php echo $URI ?>resources/js/sweetalert.min.js"></script>
<script src="<?php echo $URI ?>resources/js/select2.full.min.js"></script>
<script>
    var configJSON = '<?php echo $configJSON?>';
    var WIDGETCONFIG = JSON.parse(configJSON);
    var URI = WIDGETCONFIG.siteURI;
    var PreloaderHelper = {
        show: function () {
            $('body').append('<div class="nova_preloader">' +
                '<div class="container-preloader-evg">' +
                '<div class="circular-container">' +
                '<div class="circle circular-loader1">' +
                '<div class="circle circular-loader2"></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        },
        hide: function () {
            $('.nova_preloader').remove();
        }
    };
    var types = {
        add: 'Добавление',
        move: 'Перемещение',
        reserved: 'Резерв',
        writeoff: 'Списание',
        delete: 'Удаление'
    };

    function fullDate(timestamp = '') {
        var date = new Date(timestamp*1000);

        var year = date.getFullYear();

        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;

        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;


        var hour ="0" + date.getHours();
        // Minutes part from the timestamp
        var min = "0" + date.getMinutes();
        // Seconds part from the timestamp
        var sec = "0" + date.getSeconds();

        return day + '.' + month + '.' + year + ' ' + hour.substr(-2) + ':' + min.substr(-2) + ':' + sec.substr(-2);
    };
    function typeName(code){
        return types[code];
    }
    $(document).ready(function () {
        initFilter();
        renderTable({});

        function initFilter(){
            //Действия
            _.each(types, function (name, code) {
                $('#action_type_filter').append($('<option>', {
                    value: code,
                    text : name
                }));
            });

            $.get(URI+'stock/all', {}, function(stocks){
                _.each(stocks, function (stock) {
                    $('#stock_filter').append($('<option>', {
                        value: stock.name,
                        text : stock.name
                    }));
                })
            });

            $.get(URI+'/amo/users', {}, function(users){
                _.each(users, function (user) {
                    $('#manager_id_filter').append($('<option>', {
                        value: user.id,
                        text : user.name
                    }));
                })
            });

            $.get(URI+"items/get/all", {}, function (items) {
                console.log(items);
                var elements = [];
                _.each(items, function (item, i) {

                    var series = item.series ? item.series : '';
                    var number = item.number ? item.number : '';
                    var reestr = item.reestr ? item.reestr : '';
                    var agent = item.agent ? item.agent : '';
                    elements.push({
                        id: item.id,
                        text: series + " Номер: " + number + " Агент: " + agent,
                    });

                });
                console.log(elements);
                $('#item_filter').select2({
                    data: elements
                });
            });

        }


       function renderTable(params) {
           PreloaderHelper.show();
           $('#history-table tbody').empty();
           $.post(URI+'history/all', params, function (histories) {
                console.log(histories);
                //var tbodyHTML = '';
                _.each(histories, function (history) {
                    var date = history.date?fullDate(history.date):'';
                    var type = history.type?history.type:'';
                    var manager = history.manager_id?history.manager_id:'';
                    var stock = history.stock?history.stock:'';
                    var series = history.series?history.series:'';
                    var number = history.number?history.number:'';
                    var agent = history.agent?history.agent:'';
                    var payment_price = history.payment_price?history.payment_price:'';
                    var payment_source = history.payment_source?history.payment_source:'';
                    $('#history-table tbody').prepend(
                        '<tr>'+
                            '<th>'+date+'</th>' +
                            '<th>'+typeName(type)+'</th>' +
                            '<th>'+manager+'</th>' +
                            '<th>'+stock+'</th>' +
                            '<th>'+series+'</th>' +
                            '<th>'+number+'</th>' +
                            '<th>'+agent+'</th>' +
                            '<th>'+payment_price+'</th>' +
                            '<th>'+payment_source+'</th>'+
                        '<tr>'
                    );
                });
                PreloaderHelper.hide();
           });
       }
       $('#confirm_history_filter').click(function (e) {
           e.preventDefault();
           var params = {
               type: $('#action_type_filter').val()?$('#action_type_filter').val(): '',
               stock: $('#stock_filter').val()?$('#stock_filter').val(): '',
               item: $('#item_filter').val()?$('#item_filter').val(): '',
               manager_id: $('#manager_id_filter').val()?$('#manager_id_filter').val(): '',
               period: $('input[name="period_filter"]:checked').val()?$('input[name="period_filter"]:checked').val(): ''

           };
           console.log(params);
           renderTable(params);
       });

    });
</script>
<style>
    .nova_preloader{
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(255,255,255,.5);
        z-index: 9999999;
    }
    .container-preloader-evg{
        text-align: center;
        color: #fff;
        text-align: center;
        position: absolute;
        width: 100%;
        left: 0;
        top: 50%;
    }
    .circular-container{
        width:10%;
        margin: 0 auto;
    }

    .circle{
        border: 5px solid transparent;
        border-radius: 50%;
    }

    .circular-loader1{
        width: 300px;
        height: 300px;
        display: table;
        padding: 1px;
        border-top: 5px solid #006495;
        border-bottom: 5px solid #006495;
        animation: circular_loader1 linear 2s infinite;
    }

    .circular-loader2{
        width: 300px;
        height: 30px;
        display: table-cell;
        border-right: 5px solid #3498db;
        border-left: 5px solid #3498db;
        animation: circular_loader2 linear 2s infinite;
    }

    @keyframes circular_loader1 {
        0% {
            transform: rotate(0deg);
        }
        50% {
            transform: rotate(-90deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes circular_loader2 {
        0% {
            transform: rotate(0deg);
        }
        50% {
            transform: rotate(-180deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
</body>
</html>